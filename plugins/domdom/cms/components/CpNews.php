<?php namespace DomDom\Cms\Components;

use Cms\Classes\ComponentBase;
use DomDom\Cms\Models\NewsPage;
use DomDom\Cms\Models\Category;
use Input;

class CpNews extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'NewsPage Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $pSearch = isset($_GET['p']) ? trim($_GET['p']) : '';
        $newList = new NewsPage;
        if(!empty($pSearch)){
            $newList = $newList->where('title', 'LIKE', '%'.$pSearch.'%')
                                ->orWhere('description', 'LIKE', '%'.$pSearch.'%')
                                ->orWhere('small_description', 'LIKE', '%'.$pSearch.'%');
        }

        $this->cp_categories = Category::all();
        $this->cp_news = $newList->orderBy('created_at', 'desc')->paginate(8);
        $this->page['highlightnews'] = NewsPage::orderBy('created_at', 'desc')->where('highlight','=','1')->get();
        $this->cp_news_first_cat = NewsPage::where('category_id','1')->orderBy('created_at', 'desc')->paginate(8);

    }

    public function onTabPaginationNews(){
        $this->page['cp_news'] = NewsPage::where('category_id',Input::get('validcat'))->where('highlight','0')->orderBy('created_at', 'desc')->paginate(8);
    }

    public function defineProperties()
    {
        return [];
    }

    public $cp_categories,$cp_news,$cp_news_first_cat,$singlePost;
}
