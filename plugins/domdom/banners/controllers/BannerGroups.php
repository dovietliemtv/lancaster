<?php namespace DomDom\Banners\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Banner Groups Back-end Controller
 */
class BannerGroups extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Banners', 'banners', 'bannergroups');
    }
}
