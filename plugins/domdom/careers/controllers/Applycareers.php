<?php namespace Domdom\Careers\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DomDom\Careers\Models\Applycareer;
/**
 * Applycareers Back-end Controller
 */
class Applycareers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'applycareers');
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'file_url' ) {
            return '<a href="'.$record->file_url.'" target="_blank">'.$record->file_name.'</a>';
        }
    }
}
