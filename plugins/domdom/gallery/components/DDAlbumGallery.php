<?php 
namespace Domdom\Gallery\Components;

use Cms\Classes\ComponentBase;
use DomDom\Gallery\Models\Gallery;
use DomDom\Projects\Models\Project;
use Input;

class DDAlbumGallery extends ComponentBase
{
    public $ddAlbumGallery;
    public $ddAlbumProject;

    public function componentDetails()
    {
        return [
            'name'        => 'DD Album Gallery Component',
            'description' => 'Show Album Gallery on the website'
        ];
    }

    public function onRun(){
        $this->page['ddAlbumGallery'] = Gallery::orderBy('created_at', 'desc')->get();
        $this->page['ddAlbumProject'] = Project::orderBy('name_pj', 'asc')->get();
    }

    public function onFilterGallery(){
        if(Input::get('project') != 0){
            $ddAlbumGallery = Gallery::where('project_id',Input::get('project'))->orderBy('created_at', 'desc')->get();
        }else{
            $ddAlbumGallery = Gallery::orderBy('created_at', 'desc')->get();
        }

        $this->page['ddAlbumGallery'] = $ddAlbumGallery;
    }

    public function defineProperties()
    {
        return [];
    }
}
