<?php namespace DomDom\Careers\Models;

use Model;

/**
 * Career Model
 */
class Career extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_careers_careers';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name_career','slug_career','description_career'];

    public $rules = [
        'name_career' => 'required',
        'slug_career' => 'required|unique:domdom_careers_careers',
        'description_career' => 'required'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'category_career' => ['Domdom\Careers\Models\CareerCategory', 'key' => 'category_career_id'],
        'location' => ['Domdom\Location\Models\Location', 'key' => 'location_career'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getTypeCareerOptions() {
        return [
            '0' => 'Full Time',
            '1' => 'Part Time',
        ];
    }
}
