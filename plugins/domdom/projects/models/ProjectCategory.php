<?php namespace DomDom\Projects\Models;

use Model;

/**
 * ProjectCategory Model
 */
class ProjectCategory extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_projects_project_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
