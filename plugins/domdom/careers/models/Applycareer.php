<?php namespace Domdom\Careers\Models;

use Model;

/**
 * applycareer Model
 */
class Applycareer extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_careers_applycareers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'file_apply' => 'System\Models\File'
    ];
    public $attachMany = [];

}
