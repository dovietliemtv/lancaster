<?php namespace Domdom\Cms\Models;

use Model;

/**
 * ContactPageInfo Model
 */
class ContactPageInfo extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_contact_page_infos';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
