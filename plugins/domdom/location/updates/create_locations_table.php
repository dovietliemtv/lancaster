<?php namespace Domdom\Location\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateLocationsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_location_locations')){
            Schema::create('domdom_location_locations', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_location');
                $table->string('slug');
                $table->string('title');
                $table->string('featured_img');
                $table->timestamps();
            });
        }
//        Schema::table('domdom_location_locations', function($table)
//        {
//            $table->string('slug')->after('name_location');
//            $table->string('title')->after('slug');
//            $table->string('featured_img')->after('title');
//        });

    }

    public function down()
    {
        Schema::dropIfExists('domdom_location_locations');
    }
}
