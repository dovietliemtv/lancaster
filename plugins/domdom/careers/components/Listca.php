<?php namespace Domdom\Careers\Components;

use Cms\Classes\ComponentBase;
use DomDom\Careers\Models\Career;
use DomDom\Careers\Models\CareerCategory;
use Input;
use Mail;
use DomDom\Careers\Models\Applycareer;
use Redirect;
use Session;
use Domdom\Careers\Models\Settings;
use System\Models\File as SystemFile;

class Listca extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'List Career',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $this->page['messSucc'] = Settings::get('confirmation_text');
        $this->page['messFail'] = Settings::get('confirmation_fail_text');

        $this->cp_listca = Career::orderBy('created_at', 'desc')->paginate(4);
        $this->cp_listcaca = CareerCategory::orderBy('created_at', 'desc')->get();
        if(Session::get('message')){
            if(Session::get('message') == 1){
                $this->cp_sessionmg = 1;
            }else{
                $this->cp_sessionmg = 0;
            }
            Session::forget('message');
        }else{
            $this->cp_sessionmg = -1;
        }

    }

    public function onFilterCareer(){
        if(post('cp_cacaid') != 0){
            $cp_listca = Career::where('category_career_id',post('cp_cacaid'))->orderBy('created_at', 'desc')->get();
        }else{
            $cp_listca = Career::orderBy('created_at', 'desc')->get();
        }
        $this->page['cp_listca'] = $cp_listca;
    }

    public function onPrepareForLaunch(){
        $data = post();
        try {
            $applycareer = new Applycareer;
            $applycareer->name_apply = $data['name_pfl'];
            $applycareer->email_apply = $data['email_pfl'];
            $applycareer->message_apply = $data['message_pfl'];
            $applycareer->work_apply = $data['work_pfl'];
            $applycareer->file_url = '';
            $applycareer->file_name = '';
            $applycareer->file_apply = $pathToFile = Input::file('file_pfl');
            if($applycareer->save()){
                $careers =  SystemFile::where('field', 'file_apply')->where('attachment_id', $applycareer->id)->where('attachment_type', 'Domdom\Careers\Models\Applycareer')->first();

                if($careers){
                    $applycareer->file_url = $careers->path;
                    $applycareer->file_name = $careers->file_name;
                    $applycareer->save();
                }

                if($this->enableSentMail()){
                    Mail::send('domdom.careers::emails.message', $data, function($message) use($pathToFile, $data) {
                        $message->to( Settings::get('recipient_email'), Settings::get('recipient_name') );
                        $message->subject( Settings::get('subject') );
                        $message->attach($pathToFile, ['as' => $data['name_pfl']]);
                    });
                }

                return Redirect::to('career')->with('message', '1');
            }else{
                return Redirect::to('career')->with('message', '0');
            }     
            
        } catch(\Exception $exception) {
            return Redirect::to('career')->with('message', '0');
        }
    }

    public function defineProperties()
    {
        return [];
    }

    public function enableSentMail(){
        return Settings::get('enable_sent_mail');
    }

    public $cp_listca;
    public $cp_listcaca;
    public $cp_sessionmg;
}
