<?php namespace Domdom\Location\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDistrictsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_location_districts')){
            Schema::create('domdom_location_districts', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_district');
                $table->string('location_district');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_location_districts');
    }
}
