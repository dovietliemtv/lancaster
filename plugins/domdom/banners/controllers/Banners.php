<?php namespace DomDom\Banners\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Banners Back-end Controller
 */
class Banners extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'banners');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_banner');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'banner_image' ) {
            return '<img src="' . $record->banner_image->path . '" width="100" />';
        }
    }
}
