<?php namespace DomDom\Events\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Events Back-end Controller
 */
class Events extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'events');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_event');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image_event' ) {
            return '<img src="' . $record->featured_image_event->path . '" width="100" />';
        }
    }
}
