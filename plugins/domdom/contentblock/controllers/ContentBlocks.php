<?php namespace Domdom\ContentBlock\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Content Blocks Back-end Controller
 */
class ContentBlocks extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'contentblocks');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_contentblock');

        return $this->asExtension('FormController')->create();
    }
}
