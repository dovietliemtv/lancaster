<?php namespace DomDom\Banners\Models;

use Model;

/**
 * Banner Model
 */
class Banner extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_banners_banners';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['description_banner','title_banner','quote_banner'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
        'code_banner' => 'required|unique:domdom_banners_banners',
        'banner_image' => 'required'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'banner_image' => 'System\Models\File'
    ];
    public $attachMany = [];
}
