<?php namespace Domdom\Membership\Components;

use Cms\Classes\ComponentBase;
use Validator;
use ValidationException;
use Hash;
use DomDom\Membership\Models\Member;
use Config;
use Mail;
use Session;
use Cookie;
use Redirect;
use RainLab\Translate\Models\Message;

class Members extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Member Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun() {

        //check token if page is 'auth/reset'
        if ( $this->param('slug') == 'reset' ) {
            $this->page['token_expired'] = Member::where( 'token', get('token') )
                ->where( 'token_expired', '>', date('Y-m-d h:i:s') )
                ->count() == 0 ? true : false;
        }

        //pass 'query params' to view
        $this->page['query'] = get();

        //try 'remember'(if yes)
        $this->cookieLogin();
        //try get 'auth'
        $this->page['auth'] = Session::get('lancaster.member');
    }

    public function onLogin() {
        /*
         * Validate input
         */
        $data = post();

        $rules = [
            'email' => 'required|email',
            'passwd' => 'required'
        ];

        $validation = Validator::make($data, $rules);
        if ( $validation->fails() ) {
            throw new ValidationException( $validation );
        }

        $member = Member::where( 'email', $data['email'] )
            ->first();

        if ( count($member) > 0 && Hash::check( $data['passwd'], $member->password ) ) {
            //session
            if ( isset($data['remember']) ) {
                $cookieExpired = 60*24*7; //minute*day*weekdays
                Cookie::queue( 'lancaster_login', $data['email'], $cookieExpired );
                Cookie::queue( 'lancaster_passwd', $data['passwd'], $cookieExpired );
            }
            Session::put('lancaster.member', $member['attributes']);

//            \Flash::success('Successfully!');
            return Redirect::refresh();
        }else{
            \Flash::error('Email or Password is incorrect!');
            return [
                'error' => 'invalid_credentials',
                'message' => Message::trans('Email or Password is incorrect.')
            ];
        }
    }

    public function onLogout() {
        Session::forget('lancaster.member');

        Cookie::queue(
            Cookie::forget('lancaster_login')
        );
        Cookie::queue(
            Cookie::forget('lancaster_passwd')
        );

        unset( $this->page['auth'] );

        return Redirect::refresh();
    }

    public function onForgot() {
        /*
         * Validate input
         */
        $data = post();

        $rules = [
            'email' => 'required|email'
        ];
        $validation = Validator::make($data, $rules);
        if ( $validation->fails() ) {
            throw new ValidationException( $validation );
        }

        $user = Member::where( 'email', $data['email'] )->first();
        if ( count($user) > 0 ) {
            $token = $this->getToken( $user->login, $user->email );
            //update Token for user
            Member::where( 'email', $user->email )
                ->update([
                    'token' => $token,
                    'token_expired' => date( 'Y-m-d H:i:s', strtotime('+30 minutes') )
                ]);

            // These variables are available inside the message as Twig
            $fullname = $user->firstname . ' ' . $user->lastname;
            $vars = [
                'fullname' => $fullname,
                'login' => $user->login,
                'link' => Config::get('app.url') . '/auth/reset?token=' . $token . '&email=' . $user->email
            ];
            try {
                Mail::send('lancaster.auth.forgot', $vars, function($message) use ($user, $fullname) {
                    $message->to( $user->email, $fullname );
                });
                \Flash::success('Instructions to reset your account have been sent to your email!');
                return [
                    'success' => true,
                    'message' => Message::trans('Instructions to reset your account have been sent to your email.')
                ];
            } catch(\Exception $exception) {
                \Flash::error('Send email failed. Please try again later!');
                return [
                    'error' => 'send_email_failed',
                    'message' => Message::trans('Send email failed. Please try again later.')
                ];
            }
        }
        else {
            \Flash::error('This email has not been registered yet!');
            return [
                'error' => 'invalid_email',
                'message' => Message::trans('This email has not been registered yet.')
            ];
        }
    }

    public function onReset() {
        /*
         * Validate input
         */
        $data = post();

        $rules = [
            'token' => 'required',
            'email' => 'required|email',
            'passwd' => 'required|min:6|confirmed',
        ];

        $validation = Validator::make($data, $rules);
        if ( $validation->fails() ) {
            throw new ValidationException($validation);
        }

        if ( Member::where( 'token', $data['token'] )
            ->where( 'token_expired', '>', date('Y-m-d h:i:s') )
            ->update([
                'password' => Hash::make( $data['passwd'] ),
                'token' => null,
                'token_expired' => null
            ])
        ) {

            return [
                'success' => true,
                'message' => Message::trans('Your password has been updated.')
            ];
        }else{

            return [
                'error' => 'reset_password_failed',
                'message' => Message::trans('Password reset failed. Your token has been expired.<br />Please use forgot password function again.')
            ];
        }
    }

    public function onSignup() {
        $data = post();

        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'passwd' => 'required|min:6|confirmed',
        ];

        $validation = Validator::make($data, $rules);
        if ( $validation->fails() ) {
            throw new ValidationException( $validation );
        }
        //create member account
        $checkMemberExist = Member::where( 'email', $data['email'] );
        if ( $checkMemberExist->count() == 0 ) {
            //new member
            $member = new Member;
            $member->firstname = $data['firstname'];
            $member->lastname = $data['lastname'];
            $member->email = $data['email'];
            $member->phone = $data['phone'];
            $member->password = Hash::make( $data['passwd'] );
            if ( $member->save() ) {
                //send mail
                try {
                    $vars = [
                        'login' => $data['email'],
                        'passwd' => $data['passwd'],
                        'link' => Config::get('app.url') . '#login'
                    ];

                    Mail::send('lancaster.auth.signup', $vars, function($message) use ($data) {
                        $message->to( $data['email'] );
                    });
                } catch(\Exception $exception) {}

                Session::put('lancaster.member', $member['attributes']);

                return Redirect::refresh();
            }
        }
        else {
            \Flash::error(Message::trans( 'Email already exists', [] ));
        }
    }



    private function getToken($login, $email) {
        return md5( $login . $email . Config::get('app.key') . time() . rand() );
    }
    private function cookieLogin() {
        $login = Cookie::get('lancaster_login');
        $passwd = Cookie::get('lancaster_passwd');

        if ( isset($login) && isset($passwd) ) {
            $member = Member::where( 'email', $login )->first();
            if ( count($member) > 0 ) {
                if ( Hash::check( $passwd, $member->password ) ) {
                    Session::put( 'lancaster.member', $member['attributes'] );

                    return true;
                }
            }
        }
        return false;
    }

    public static function getAuth() {
        return Session::has('lancaster.member') ? Session::get('lancaster.member') : false;
    }
}
