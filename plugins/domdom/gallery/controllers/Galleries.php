<?php
namespace DomDom\Gallery\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DomDom\Gallery\Models\Gallery;

/**
 * Seo Multi Langs Back-end Controller
 */
class Galleries extends Controller {
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct() {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'library_list');
    }

    //CMS actions
    public function index() {
        return $this->asExtension('ListController')->index();
    }
    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_library');

        return $this->asExtension('FormController')->create();
    }

    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && is_array($checkedIds) && count($checkedIds)) {

            foreach ($checkedIds as $postId) {
                if ((!$post = Gallery::find($postId))) {
                    continue;
                }

                $post->delete();
            }

            Flash::success('Successfully deleted those gallaries.');
        }

        return $this->listRefresh();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image' ) {
            return '<img src="' . $record->featured_image->path . '" width="100" />';
        }
    }
}