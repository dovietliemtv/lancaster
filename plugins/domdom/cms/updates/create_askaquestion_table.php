<?php
namespace DomDom\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateAllTables extends Migration {

    public function up() {
        if ( !Schema::hasTable('domdom_projects_askaquestion') ) {
            Schema::create('domdom_projects_askaquestion', function($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->string('phone');
                $table->smallInteger('type');
                $table->text('message');
                $table->timestamps();
            });
        }
    }
    public function down() {
        Schema::dropIfExists('domdom_projects_askaquestion');
    }

}