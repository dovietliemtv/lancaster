<?php namespace Domdom\Events\Components;

use Cms\Classes\ComponentBase;
use DomDom\Events\Models\Event;
use DomDom\Events\Models\RegistrationEvent;
use DomDom\Membership\Components\Members;
use DomDom\Events\Models\SaveEvent;
use Config;

class Detailev extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Detail Event',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $this->cp_detailev = Event::where( 'slug_event', $this->property('cpslugevent') )->first();
        if(Members::getAuth()['id']){
            $this->cp_checkse =  SaveEvent::where('member_id',Members::getAuth()['id'])->where('event_id',$this->cp_detailev->id)->count();
            $this->cp_checkre = RegistrationEvent::where('member_id',Members::getAuth()['id'])->where('event_id',$this->cp_detailev->id)->count();
        }else{
            $this->cp_checkse = -1;
            $this->cp_checkre = -1;
        }

        $this->page['sharewebsiteurl'] = Config::get('app.url') . '/event/' . $this->cp_detailev->slug_event;
        $this->page['sharewebsitetitle'] = $this->cp_detailev->name_event;
        $this->page['sharewebsitedescription'] = $this->cp_detailev->description_event;
        $this->page['sharewebsiteimage'] = $this->cp_detailev->featured_image_event;

    }

    public function onRegistrationEvent(){

            if(Members::getAuth()['id']){
                if(post('cp_unreev') == 1){
                    RegistrationEvent::where('member_id',Members::getAuth()['id'])->where('event_id',post('cp_detailevid'))->delete();
                }else{
                    $cp_re = new RegistrationEvent;
                    $cp_re->member_id = Members::getAuth()['id'];
                    $cp_re->event_id = post('cp_detailevid');
                    $cp_re->save();
                }
                \Flash::success('Successfully!');
                return [
                    'success' => true
                ];
            }else{
                \Flash::error('You must login!');
                return [
                    'success' => false
                ];
            }

    }

    public function onSaveEvent(){
        if(Members::getAuth()['id']) {
            if(post('cp_unsaveev') == 1){
                SaveEvent::where('member_id',Members::getAuth()['id'])->where('event_id',post('cp_detailevid'))->delete();
            }else{
                $cp_se = new SaveEvent;
                $cp_se->member_id = Members::getAuth()['id'];
                $cp_se->event_id = post('cp_detailevid');
                $cp_se->timestamps = false;
                $cp_se->save();
            }
            \Flash::success('Successfully!');
            return [
                'success' => true
            ];
        }else{
            \Flash::error('You must login!');
            return [
                'success' => false
            ];
        }
    }

    public function defineProperties()
    {
        return [
            'cpslugevent' => [
                'title'             => 'Slug',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_detailev;
    public $cp_checkse;
    public $cp_checkre;
}
