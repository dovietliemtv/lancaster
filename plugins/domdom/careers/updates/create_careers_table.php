<?php namespace DomDom\Careers\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCareersTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_careers_careers')){
            Schema::create('domdom_careers_careers', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_career');
                $table->string('slug_career')->unique();
                $table->text('description_career')->nullable();
                $table->string('location_career');
                $table->string('type_career')->nullable();
                $table->integer('category_career_id');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_careers_careers');
    }
}
