<?php namespace DomDom\DistrictGuides\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDistrictGuidesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_districtguides_district_guides')){
            Schema::table('domdom_districtguides_district_guides', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_dg');
                $table->string('slug_dg')->unique();
                $table->string('region_dg');
                $table->text('main_description_dg')->nullable();
                $table->text('description_dg')->nullable();
                $table->text('quote_dg')->nullable();
                $table->text('description_around_dg')->nullable();
                $table->json('feature_dg')->nullable();
                $table->timestamps();
                $table->string('short_description_dg')->nullable();
                $table->text('lat');
                $table->text('lng');
                $table->text('buttons_custom');
            });
        }
        Schema::table('domdom_districtguides_district_guides', function($table)
        {
            $table->text('buttons_custom');

        });

    }

    public function down()
    {
        Schema::dropIfExists('domdom_districtguides_district_guides');
    }
}
