<?php namespace Domdom\Homes\Components;

use Cms\Classes\ComponentBase;
use DomDom\Homes\Models\Home;
use DomDom\Homes\Models\SaveHome;
use DomDom\Membership\Components\Members;
use Config;

class Detailho extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Detail Home',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $this->cp_detailho = Home::where( 'slug_home', $this->property('cpslughome') )->first();
        if(Members::getAuth()['id']){
            $this->cp_checksh =  SaveHome::where('member_id',Members::getAuth()['id'])->where('home_id',$this->cp_detailho->id)->count();
        }else{
            $this->cp_checksh = -1;
        }

        $this->page['sharewebsiteurl'] = Config::get('app.url') . '/find-your-home/' . $this->cp_detailho->slug_home;
        $this->page['sharewebsitetitle'] = $this->cp_detailho->name_home;
        $this->page['sharewebsitedescription'] = $this->cp_detailho->description_home;
        $this->page['sharewebsiteimage'] = $this->cp_detailho->featured_image_home;

    }

    public function onSaveHome(){
        if(Members::getAuth()['id']){
            if(post('cp_unsaveho') == 1){
                SaveHome::where('member_id',Members::getAuth()['id'])->where('home_id',post('cp_detailhoid'))->delete();
            }else{
                $cp_sh = new SaveHome;
                $cp_sh->member_id = Members::getAuth()['id'];
                $cp_sh->home_id = post('cp_detailhoid');
                $cp_sh->timestamps = false;
                $cp_sh->save();
            }
            \Flash::success('Successfully!');
            return [
                'success' => true
            ];
        }else{
            \Flash::error('You must login!');
            return [
                'success' => false
            ];
        }

    }

    public function defineProperties()
    {
        return [
            'cpslughome' => [
                'title'             => 'Slug',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_detailho;
    public $cp_checksh;
}
