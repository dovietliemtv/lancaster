<?php namespace DomDom\Events\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Save Events Back-end Controller
 */
class SaveEvents extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Events', 'events', 'saveevents');
    }
}
