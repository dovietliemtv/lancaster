<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;
use DomDom\Projects\Models\Project;
use DomDom\Homes\Models\Home;
use DomDom\Homes\Models\SaveHome;
use DomDom\Membership\Components\Members;
use DomDom\Location\Models\Location;

class Homepage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Home Page Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $this->cp_homepj = Project::where('starred', 1)->orderBy('created_at', 'desc')->get();
        $this->cp_homeho = Home::orderBy('created_at', 'desc')->get();
        $this->cp_listsh = SaveHome::all();


        $this->page['location_district_guide'] = Location::all();

    }

    public function onSaveHome(){
        if(Members::getAuth()['id']){
            if(post('cp_unsaveho') == 0){
                $cp_sh = new SaveHome;
                $cp_sh->member_id = Members::getAuth()['id'];
                $cp_sh->home_id = post('cp_detailhoid');
                $cp_sh->timestamps = false;
                $cp_sh->save();
            }else{
                SaveHome::where('member_id',Members::getAuth()['id'])->where('home_id',post('cp_detailhoid'))->delete();
            }
            \Flash::success('Successfully!');
            return [
                'success' => true
            ];
        }else{
            \Flash::error('You must login!');
            return [
                'success' => false
            ];
        }

    }

    public function defineProperties()
    {
        return [];
    }

    public $cp_homepj;
    public $cp_homeho;
    public $cp_listsh;
}
