<?php namespace DomDom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateNewsPagesTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('domdom_cms_news_pages') ) {
            Schema::create('domdom_cms_news_pages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->integer('category_id');
                $table->timestamps();
                $table->string('slug');
                $table->boolean('highlight');
                $table->text('small_description');
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_news_pages');
    }
}
