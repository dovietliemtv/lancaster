<?php 
namespace DomDom\Gallery;

use Backend;
use System\Classes\PluginBase;

/**
 * Gallery Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'DD Gallery',
            'description' => 'Plugin to enable management of gallery on website.',
            'author'      => 'DomDom',
            'icon'        => 'icon-picture-o'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'DomDom\Gallery\Components\DDAlbumGallery' => 'ddAlbumGallery',
            'DomDom\Gallery\Components\DDAlbumGalleryDetail' => 'ddAlbumGalleryDetail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'domdom.gallery.access_manage_gallery' => [
                'tab' => 'DD Gallery',
                'label' => 'Manage the gallery'
            ]
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

    }
}
