<?php namespace DomDom\Projects\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Projects Back-end Controller
 */
class Projects extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'projects');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_project');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image_pj' ) {
            return '<img src="' . $record->featured_image_pj->path . '" width="100" />';
        }
        if ( $columnName == 'starred' ) {
            return '<div style="width: 60px;text-align:center;">' . ( $record->starred ? '<i class="icon-star" style="color:#f0ad4e;font-size:18px;"></i>' : '<i class="icon-star-o"></i>' ) . '</div>';
        }
    }
}
