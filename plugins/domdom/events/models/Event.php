<?php namespace DomDom\Events\Models;

use Model;

/**
 * Event Model
 */
class Event extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_events_events';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name_event','slug_event','description_event','location_event'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
        'name_event' => 'required',
        'slug_event' => 'required|unique:domdom_events_events',
        'location_event' => 'required',
        'featured_image_event' => 'required',
        'start_time' => 'required',
        'end_time' => 'required'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    public function afterFetch() {
        $this->is_closed = $this->end_time < date('Y-m-d H:i:s') ? 1 : 0;
        $this->is_opened = $this->start_time < date('Y-m-d H:i:s') ? 1 : 0;
    }
    public function beforeSave() {
        unset( $this->is_closed );
        unset( $this->is_opened );
    }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image_event' => 'System\Models\File'
    ];
    public $attachMany = [
        'photos_event' => 'System\Models\File'
    ];
}
