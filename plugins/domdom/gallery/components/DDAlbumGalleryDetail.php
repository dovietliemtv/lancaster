<?php 
namespace DomDom\Gallery\Components;

use Cms\Classes\ComponentBase;
use DomDom\Gallery\Models\Gallery;

class DDAlbumGalleryDetail extends ComponentBase
{
    public $ddGalleryDetail;

    public function componentDetails()
    {
        return [
            'name'        => 'DD Album Gallery Detail Component',
            'description' => 'Show Album Gallery Detail on the website.'
        ];
    }

    public function onRun(){
        $this->ddGalleryDetail  = $this->page['ddGalleryDetail'] = Gallery::where( 'slug', $this->property('slug') )->first();
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'             => 'Slug',
                'type'              => 'string'
            ]
        ];
    }
}
