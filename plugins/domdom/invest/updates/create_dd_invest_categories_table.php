<?php 
namespace DomDom\Invest\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDDInvestCategoriesTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('domdom_invest_categories') ) {
            Schema::create('domdom_invest_categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->string('slug');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        if ( Schema::hasTable('domdom_invest_categories') ) {
            Schema::dropIfExists('domdom_invest_categories');
        }
    }
}
