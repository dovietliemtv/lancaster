<?php 
namespace DomDom\Invest\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDDInvestsTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('domdom_invests') ) {
            Schema::create('domdom_invests', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->text('description');
                $table->integer('category_id');
                $table->timestamps();
                $table->string('slug');
            });
        }
    }

    public function down()
    {
        if ( Schema::hasTable('domdom_invests') ) {
            Schema::dropIfExists('domdom_invests');
        }
    }
}
