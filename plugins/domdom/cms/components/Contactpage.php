<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;
use Domdom\Cms\Models\ContactPageInfo;

class Contactpage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact Page Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $this->page['contactPageInfo'] = ContactPageInfo::first();
    }

    public function defineProperties()
    {
        return [];
    }
}
