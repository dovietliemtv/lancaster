<?php namespace DomDom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('domdom_cms_categories') ) {
            Schema::create('domdom_cms_categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->string('slug');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_categories');
    }
}
