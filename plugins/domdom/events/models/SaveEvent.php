<?php namespace DomDom\Events\Models;

use Model;

/**
 * SaveEvent Model
 */
class SaveEvent extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_events_save_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
