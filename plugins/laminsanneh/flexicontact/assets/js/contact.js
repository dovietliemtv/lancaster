'use strict';

(function($) {
    $(document).ready(function() {
        var contactForm = $('#contact-form');
        contactForm.isHappy({
            fields: {
                '#contact-name': {
                    required: true,
                },
                '#contact-email': {
                    required: true,
                    test: happy.email
                },
                '#contact-number': {
                    required: true,
                },
                '#contact-message': {
                    required: true,
                },
            },
            happy: function() {
                $.request( contactForm.attr('data-form-request'), {
                    data: {
                        'contactName': $('#contact-name').val(),
                        'contactEmail': $('#contact-email').val(),
                        'contactNumber': $('#contact-number').val(),
                        'contactMessage': $('#contact-message').val(),
                    },
                    success: function(response) {
                        var result = $('#contact-result');
                        result.removeClass('error').removeClass('success');
                        if ( response.success ) {
                            result.addClass('success');
                        }
                        else {
                            result.addClass('error');   
                        }
                        result.html( response.message );

                        // hide the form
                        contactForm.find('.dd-form-row').hide().eq(0).show();
                        contactForm.find('.dd-form-btn').hide();
                    }
                } );

                return false;
            },
            unHappy: function() {
                return false;
            }
        });
    });
})(jQuery);