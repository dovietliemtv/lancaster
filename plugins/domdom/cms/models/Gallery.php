<?php namespace Domdom\Cms\Models;

use Model;

/**
 * Gallery Model
 */
class Gallery extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_galleries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['video_gallery'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [
        'image' => 'System\Models\File'
    ];
}
