<?php namespace DomDom\Careers;

use Backend;
use System\Classes\PluginBase;

/**
 * Careers Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Career',
            'description' => 'No description provided yet...',
            'author'      => 'DomDom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'DomDom\Careers\Components\Listca' => 'Listca',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.careers.some_permission' => [
                'tab' => 'Careers',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'careers' => [
                'label'       => 'Careers',
                'url'         => Backend::url('domdom/careers/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.careers.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Carrer Apply Settings',
                'description' => 'Manage the settings for the carrer apply',
                'category'    => 'Marketing',
                'icon'        => 'icon-file',
                'class'       => 'DomDom\Careers\Models\Settings',
                'permissions' => ['domdom.careers.access_settings'],
                'order'       => 102
            ]
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'domdom.careers::emails.message' => 'Email sent to the administrator after user filled out the career form',
        ];
    }
}
