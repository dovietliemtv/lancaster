<?php

namespace DomDom\Careers\Models;

use Model;
class Settings extends Model{

    public $implement = [
        'System.Behaviors.SettingsModel'
    ];

    public $settingsCode = 'domdom_careers_apply_settings';

    public $settingsFields = 'fields.yaml';
} 