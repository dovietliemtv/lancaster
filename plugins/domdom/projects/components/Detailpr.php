<?php namespace Domdom\Projects\Components;

use Cms\Classes\ComponentBase;
use DomDom\Projects\Models\Project;

class Detailpr extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Detail Project',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $this->cp_detailpr = Project::where( 'slug_pj', $this->property('cpslugproject') )->first();
    }

    public function defineProperties()
    {
        return [
            'cpslugproject' => [
                'title'             => 'Slug',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_detailpr;
}
