<?php namespace Domdom\Homes\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Homes Back-end Controller
 */
class Homes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'homes');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_home');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image_home' ) {
            return '<img src="' . $record->featured_image_home->path . '" width="100" />';
        }
        if ( $columnName == 'listing_price_home' ) {
            return number_format( $record->listing_price_home, 0, '.', ',' ) . 'đ';
        }
    }
}
