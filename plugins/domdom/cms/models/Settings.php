<?php

namespace DomDom\Cms\Models;

use Model;
class Settings extends Model{

    public $implement = [
        'System.Behaviors.SettingsModel'
    ];

    public $settingsCode = 'domdom_cms_askaquestion_settings';

    public $settingsFields = 'fields.yaml';
} 