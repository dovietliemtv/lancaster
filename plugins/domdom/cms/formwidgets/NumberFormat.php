<?php namespace Domdom\Cms\FormWidgets;

use Backend\Classes\FormWidgetBase;

/**
 * NumberFormat Form Widget
 */
class NumberFormat extends FormWidgetBase
{
    /**
     * @inheritDoc
     */
    protected $defaultAlias = 'domdom_cms_number_format';

    /**
     * @inheritDoc
     */
    public function init()
    {
    }

    /**
     * @inheritDoc
     */
    public function render()
    {
        $this->vars['field'] = $this->formField;
        $this->prepareVars();
        return $this->makePartial('numberformat');
    }

    /**
     * Prepares the form widget view data
     */
    public function prepareVars()
    {
        $this->vars['name'] = $this->formField->getName();
        $this->vars['value'] = $this->getLoadValue();
        $this->vars['model'] = $this->model;
    }

    /**
     * @inheritDoc
     */
    public function loadAssets()
    {
        $this->addCss('css/numberformat.css', 'core');
        $this->addJs('js/accounting.js', 'core');
        $this->addJs('js/numberformat.js', 'core');
    }

    /**
     * @inheritDoc
     */
    public function getSaveValue($value)
    {
        return $value;
    }
}
