<?php namespace Domdom\Homes\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSaveHomesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_homes_save_homes')){
            Schema::create('domdom_homes_save_homes', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('home_id')->unsigned();
                $table->integer('member_id')->unsigned();
                $table->primary(['home_id', 'member_id']);
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_homes_save_homes');
    }
}
