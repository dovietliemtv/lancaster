<?php namespace Domdom\Homes\Models;

use Model;
use DomDom\Location\Models\Location;
use DomDom\Location\Models\District;

/**
 * home Model
 */
class Home extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_homes_homes';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name_home','slug_home','property_type_home','building_type_home','building_age_home','description_home','amenity_home','note_home'];

    public $rules = [
        'name_home' => 'required',
        'slug_home' => 'required|unique:domdom_homes_homes',
        'featured_image_home' => 'required',
        'baths_home' => 'numeric',
        'minimum_down_payment_home' => 'numeric',
        'lot_size_home' => 'numeric',
        'floor_area_home' => 'numeric',
        'units_in_building_home' => 'numeric',

        'listing_price_home' => 'numeric',
        'interest_rate_home' => 'numeric',
        'est_monthly_payment_home' => 'numeric',
        'term_home' => 'numeric',
        'construction_home' => 'required',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $jsonable = ['amenities'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'location' => ['Domdom\Location\Models\Location', 'key' => 'location_id_home'],
        'district' => ['Domdom\Location\Models\District', 'key' => 'district_home']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image_home' => 'System\Models\File',
        'construction_home' => 'System\Models\File',
        'file_home' => 'System\Models\File',
    ];
    public $attachMany = [
        'gallery_home' => 'System\Models\File'
    ];

    public function getTypeHomeOptions() {
        return [
            '0' => 'Rent',
            '1' => 'Buy',
        ];
    }

    public function beforeValidate() {
        $this->listing_price_home = intval( str_replace( ',', '', $this->listing_price_home ) );
        $this->est_monthly_payment_home = intval( str_replace( ',', '', $this->est_monthly_payment_home ) );
        $this->downpayment_home = intval( str_replace( ',', '', $this->downpayment_home ) );
    }

    public function getLocationIdHomeOptions()
    {
        $LocationOptions = Location::orderBy('created_at', 'desc')->get();

        $LocationArray = array();

        foreach ( $LocationOptions as $LocationOption){
            $LocationArray[$LocationOption->id] = $LocationOption->name_location;
        }
        return $LocationArray;
    }

    public function getDistrictHomeOptions()
    {
        $DistrictArray = array();
        if($this->location_id_home){
            $DistrictOptions = District::orderBy('created_at', 'desc')->where('location_district',$this->location_id_home)->get();
        }else{
            $DefaultLocationOption = Location::orderBy('created_at', 'desc')->first();
            $DistrictOptions = District::orderBy('created_at', 'desc')->where('location_district',$DefaultLocationOption->id)->get();
        }

        foreach ( $DistrictOptions as $DistrictOption){
            $DistrictArray[$DistrictOption->id] = 'District ' .$DistrictOption->name_district;
        }

        return $DistrictArray;
    }

}
