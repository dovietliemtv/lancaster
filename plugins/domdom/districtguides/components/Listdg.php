<?php namespace Domdom\Districtguides\Components;

use Cms\Classes\ComponentBase;
use DomDom\Location\Models\Location;
use DomDom\Districtguides\Models\DistrictGuide;
use Input;

class Listdg extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'District Guide',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){

        $this->cp_listdglc = Location::orderBy('created_at', 'desc')->get();

        if (empty(!$_GET)) {
            $location = $_GET['tp'];
            $this->page['selectedID'] = Location::where('slug', $location)->lists('id')[0];

            $this->cp_listdg = DistrictGuide::where('region_dg', $this->page['selectedID'] )->orderBy('created_at', 'desc')->get();
        }else{
            $this->cp_listdg = DistrictGuide::orderBy('created_at', 'desc')->get();
        }

    }

    public function onFilterDistrictGuide(){
        if(Input::get('dglc') != 0){
            $cp_listdg = DistrictGuide::where('region_dg',Input::get('dglc'))->orderBy('created_at', 'desc')->get();
        }else{
            $cp_listdg = DistrictGuide::orderBy('created_at', 'desc')->get();
        }

        $this->page['cp_listdg'] = $cp_listdg;
    }

    public function defineProperties()
    {
        return [];
    }

    public $cp_listdg;
    public $cp_listdglc;
}
