<?php namespace LaminSanneh\FlexiContact\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Members Back-end Controller
 */
class Contacts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'contacts');
    }
    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'request' ) {
            $limit = 100;
            return strlen($record->request) >= $limit ? ( substr($record->request, 0, $limit) . '...' ) : $record->request;
        }
    }
}