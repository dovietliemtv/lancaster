<?php namespace DomDom\Careers\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCareerCategoriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_careers_career_categories')){
            Schema::create('domdom_careers_career_categories', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_category_career');
                $table->string('slug_category_career')->unique();
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_careers_career_categories');
    }
}
