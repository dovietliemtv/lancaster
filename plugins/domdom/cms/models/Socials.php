<?php

namespace DomDom\Cms\Models;

use Model;
class Socials extends Model{

    public $implement = [
        'System.Behaviors.SettingsModel'
    ];

    public $settingsCode = 'domdom_cms_social_settings';

    public $settingsFields = 'fields.yaml';
} 