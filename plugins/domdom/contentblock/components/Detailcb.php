<?php namespace Domdom\Contentblock\Components;

use Cms\Classes\ComponentBase;
use DomDom\ContentBlock\Models\ContentBlock;

class Detailcb extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Content Block',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $this->cp_cb = ContentBlock::where('code_contentblock', $this->property('codecontentblock'))->first();
    }

    public function defineProperties()
    {
        return [
            'codecontentblock' => [
                'title'             => 'Code',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_cb;
}
