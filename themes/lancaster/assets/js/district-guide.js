// let obj, svg, svgOriginalWidth, svgOriginalHeight, paper, curImgage, curve, winWidth, winHeight, scaleRatio = {x:0, y:0}, gapX = 0, gapY = 0, imgOriginalWidth, imgOriginalHeight, imgCurWidth, imgCurHeight, scaleCurveRatio, curveY, mask, bg, changePhotoTimeout;
//
// let win = $(window);
// let districtInfo = $('.district-info');
// let colors = {
//     red: '#ff0000',
//     white: '#ffffff',
//     black: '#000000',
//     dot: '#FFFFFF',
//     navi: '#003349'
// };
//
// let images = [{
// 	'src': 'themes/lancaster/assets/images/quan-1.jpg',
// 	'title': 'Lancaster Legacy',
// 	'district': 1
// },{
// 	'src': 'themes/lancaster/assets/images/quan-2.jpg',
// 	'title': 'Lancaster Eden',
// 	'district': 2
// },{
// 	'src': 'themes/lancaster/assets/images/quan-4.jpg',
// 	'title': 'Lancaster Lincoln',
// 	'district': 4
// },{
// 	'src': 'themes/lancaster/assets/images/quan-5.jpg',
// 	'title': '',
// 	'district': 5
// },{
// 	'src': 'themes/lancaster/assets/images/quan-7.jpg',
// 	'title': '',
// 	'district': 7
// },{
// 	'src': 'themes/lancaster/assets/images/quan-10.jpg',
// 	'title': '',
// 	'district': 10
// },{
// 	'src': 'themes/lancaster/assets/images/quan-tan-phu.jpg',
// 	'title': '',
// 	'district': 'Tân Phú'
// }];
//
// let imgRadius = 1500;
// let maskRadius = 210;
// let dotRadius = 8;
// let dotBorderRadius = 25;
// let dots = [];
// let dotDatas = [{
// 	s: 0.1
// },{
// 	s: 0.3
// },{
// 	s: 0.5
// },{
// 	s: 0.7
// },{
// 	s: 0.9
// },{
// 	s: 1.1
// },{
// 	s: 1.3
// }];
//
// let dotBorders = [];
// let districts = [];
// let waves = Raphael.animation({r: dotBorderRadius, opacity: 0}, 1500).repeat(Infinity);
//
// let alignTop = (t) => {
//     var b = t.getBBox({
//     	width: 100
//     });
//     var h = Math.abs(b.y2) - Math.abs(b.y) + 1;
//
//     t.attr({
//         'y': b.y + h
//     });
// }
//
// let init = () => {
// 	winWidth = win.width();
// 	winHeight = 818;
// 	svgOriginalWidth = 1820;
// 	svgOriginalHeight = 818;
// 	obj = $('#district-guides');
// 	paper = new Raphael("district-guides", svgOriginalWidth, svgOriginalHeight);
// 	paper.addGuides();
// 	paper.setViewBox(0, 0, svgOriginalWidth, svgOriginalHeight);
// 	svg = obj.find('svg').eq(0);
// 	fitSvg();
//
// 	let duration = 500;
// 	let activeIndex = 2;
// 	let activeImg = images[activeIndex].src;
// 	dotDatas[activeIndex].r = dotRadius;
//
// 	curImgage = paper.circle(910, 560, 0).attr({
// 		'fillfit': 'url(' + activeImg + ')',
// 		'stroke': 'none'
// 	});
//
// 	bg = paper.circle(910, 560, 0).attr({
// 		'fill': colors.navi,
// 		'fill-opacity': 0.6,
// 		'stroke': 'none'
// 	});
//
// 	mask = paper.circle(910, 560, 0).attr({
// 		'fillfit': 'url(' + activeImg + ')',
// 		'stroke': 'none'
// 	});
//
// 	curImgage.animate({
// 		'r': imgRadius
// 	}, duration, "ease-in");
//
// 	bg.animate({
// 		'r': imgRadius
// 	}, duration, "ease-in");
//
// 	mask.animate({
// 		'r': maskRadius
// 	}, duration, "ease-in");
//
// 	curve = paper.path("M0 505L2000 505").attr({
// 		'stroke': colors.white
// 	});
//
// 	curve.animate({
// 		'path': ("M0.3,474.8c0,0,199.5-147.4,400.2,18.8c82.4,68.2,247.8-87.4,348.3-18.8c42.6,29.1,101.7,67.1,161.8,86.9c87.4,21.9,128.8,22.7,183.6-66.9c118.8-194.1,568.3-20,726.2-20")
// 	}, 500, "linear", function() {
// 		for(let i in dotDatas) {
// 			dotBorders[i] = paper.circle(0, 0, dotRadius).attr({
// 				'fill': 'none',
// 				'stroke': colors.dot,
// 				'stroke-dasharray': '- '
// 			}).attr({
// 				guide: curve,
// 				along: dotDatas[i].s
// 			});
//
//
// 			dots[i] = paper.circle(0, 0, dotRadius).attr({
// 				'fill': colors.dot,
// 				'stroke': 'none'
// 			}).attr({
// 				guide: curve,
// 				along: dotDatas[i].s,
// 				cursor: 'pointer'
// 			}).click(function() {
// 				changeDot(i, 750);
// 				clearTimeout(changePhotoTimeout);
// 				changeImg(images[i].src, 750);
// 			});
//
// 			districts[i] = paper.text(0, 45, 'District ' + images[i].district).attr({
// 				guide: curve,
// 				along: dotDatas[i].s,
// 				fill: colors.white,
// 				'font-size': 15,
// 				'font-family': 'Montserrat',
// 				'font-weight': 200
// 			});
// 		}
//
// 		// districtLabel = paper.text(660, 570, 'DỰ ÁN').attr({
// 		// 	fill: colors.white,
// 		// 	'font-size': 14,
// 		// 	'font-family': 'Montserrat',
// 		// 	'font-weight': 300,
// 		// 	'text-anchor': 'start'
// 		// });
//
// 		// districtTitle = paper.text(660, 580, 'Project Name Lorem Ipsum').attr({
// 		// 	fill: colors.white,
// 		// 	'font-size': 20,
// 		// 	'font-family': 'Montserrat',
// 		// 	'font-weight': 400,
// 		// 	'text-anchor': 'start'
// 		// });
//
// 		// alignTop(districtTitle);
//
// 		// districtIcon = paper.path('M600 570L640 570').attr({
// 		// 	'stroke': colors.white
// 		// });
//
// 		changeDot(2, 100);
// 	});
// }
//
// let changeDot = (i, duration) => {
// 	let centerPoint = 0.5;
// 	let curAlong = dots[i].attr("along");
// 	let gapToMove = centerPoint - curAlong;
//
// 	for(let k in dotDatas) {
// 		dotBorders[k].stop().animate({opacity: 1, r: dotRadius - 3}, duration, "ease-in-out", function() {
// 			let r, color, along, opac;
// 			dotDatas[k].s = dotDatas[k].s + gapToMove;
// 			along = dotDatas[k].s;
// 			if(along <= 0 || along >= 1) {
// 				opac = 0;
// 			} else if(along == centerPoint) {
// 				opac = 1;
// 			} else {
// 				opac = 1;
// 			}
// 			dots[k].animate({opacity: opac, along: along}, duration, "ease-in-out");
// 			districts[k].animate({opacity: opac, along: along}, duration, "ease-in-out");
// 			this.animate({opacity: opac, along : along}, duration, "ease-in-out", function() {
// 				// this.animate({'opacity': 1, 'r': dotBorderRadius}, duration, "ease-in-out");
// 				this.animate(waves);
// 			});
// 		});
// 	}
//
// 	districtInfo.animate({
// 		'opacity': 0
// 	}, 750, function() {
// 		$(this).find('.project-name').html(images[i].title).end().delay(500).animate({
// 			'opacity': 1
// 		}, 500)
// 	});
// }
//
// let changeImg = (nextImg, duration) => {
// 	bg.animate({
// 		'r': 0
// 	}, duration, "ease-in-out");
//
// 	mask.animate({
// 		'r': 0
// 	}, duration, "ease-in-out", function() {
// 		this.attr({
// 			'fillfit': 'url(' + nextImg + ')'
// 		});
// 	});
//
// 	curImgage.animate({
// 		'r': 0
// 	}, duration, "ease-in-out", function() {
// 		this.attr({
// 			'fillfit': 'url(' + nextImg + ')'
// 		});
// 	});
//
// 	changePhotoTimeout = setTimeout(() => {
// 		bg.animate({
// 			'r': imgRadius
// 		}, duration - 300, "ease-in-out");
// 		curImgage.animate({
// 			'r': imgRadius
// 		}, duration - 300, "ease-in-out");
// 		mask.animate({
// 			'r': maskRadius
// 		}, duration - 300, "ease-in-out");
// 	}, duration + 200);
// }
//
// let fitSvg = () => {
// 	scaleRatio.x = winWidth / svgOriginalWidth;
// 	scaleRatio.y = winHeight / svgOriginalHeight;
// 	let fitRatio = (scaleRatio.x > scaleRatio.y)?scaleRatio.x:scaleRatio.y;
// 	let fitLeftGap = (fitRatio * svgOriginalWidth - winWidth) / 2;
// 	let fitTopGap = (fitRatio * svgOriginalHeight - winHeight) / 2;
// 	svg.css({
// 		// 'zoom': fitRatio,
// 		'transform': 'scale(' + fitRatio + ')',
// 		'margin-left': -fitLeftGap,
// 		'margin-top': -fitTopGap
// 	});
//
// 	// if(winWidth < imgCurWidth) {
// 	// 	gapX = (imgCurWidth - winWidth) / 2;
// 	// 	gapY = 0;
// 	// } else {
// 	// 	scaleRatio.x = winWidth / imgCurWidth;
// 	// 	imgCurWidth = winWidth;
// 	// 	imgCurHeight = imgCurHeight * scaleRatio.x;
// 	// 	gapX = 0;
// 	// 	gapY = (imgCurHeight - winHeight) / 2;
// 	// }
// }
//
// let handleWindowResize = () => {
// 	winWidth = win.width();
// 	winHeight = 818;
//
// 	// paper.setSize(winWidth, winHeight);
// 	fitSvg();
// }
//
// window.addEventListener('load', init, false);
// window.addEventListener('resize', handleWindowResize, false);