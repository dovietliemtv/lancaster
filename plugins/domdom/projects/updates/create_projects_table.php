<?php namespace DomDom\Projects\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('domdom_projects_projects') ) {
            Schema::table('domdom_projects_projects', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_pj');
                $table->string('slug_pj')->unique();
                $table->string('short_description_pj')->nullable();
                $table->text('description_pj')->nullable();
                $table->json('feature_pj')->nullable();
                $table->timestamps();
                $table->text('quote_pj')->nullable();
            });
        }
        else {
            if ( !Schema::hasColumn('domdom_projects_projects', 'link_pj') ) {
                Schema::table('domdom_projects_projects', function($table) {
                    $table->text('link_pj')->after('slug_pj');
                });
            }

            if ( !Schema::hasColumn('domdom_projects_projects', 'starred') ) {
                Schema::table('domdom_projects_projects', function($table) {
                    $table->boolean('starred')->after('id')->nullable()->default(0);
                });
            }
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_projects_projects');
    }
}
