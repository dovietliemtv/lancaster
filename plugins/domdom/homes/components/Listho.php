<?php namespace Domdom\Homes\Components;

use Cms\Classes\ComponentBase;
use DomDom\Homes\Models\Home;
use DomDom\Homes\Components\Detailho;
use DomDom\Membership\Components\Members;
use DomDom\Homes\Models\SaveHome;
use Input;
use DomDom\Location\Models\Location;
use DomDom\Location\Models\District;
use Session;

class Listho extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'List Home',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $val_search_type = (isset($_GET['type'])) ? $_GET['type'] : '';
        $val_search_district = (isset($_GET['district'])) ? explode(",", $_GET['district']) : '';
        $val_search_bathroom = (isset($_GET['bathroom'])) ? $_GET['bathroom'] : '';
        $val_search_bedroom = (isset($_GET['bedroom'])) ? $_GET['bedroom'] : '';
        $val_search_minimum = (isset($_GET['minimum']) && trim($_GET['minimum']) != 'undefined' ) ? trim($_GET['minimum']) : '';
        $val_search_maximum = (isset($_GET['maximum']) && trim($_GET['maximum']) != 'undefined' ) ? trim($_GET['maximum']) : '';

        $resultsearch = $this->FindHome($val_search_type,$val_search_district,$val_search_bathroom,$val_search_bedroom,$val_search_minimum,$val_search_maximum);

        $this->cp_listho = $resultsearch['resulthome'];
        $this->cp_listsh = SaveHome::all();
        $this->cp_listdtho = District::orderBy('created_at', 'desc')->get();
        $this->cp_listlcho = Location::orderBy('created_at', 'desc')->get();

        $this->cp_searchtype = $resultsearch['searchtype'];
        $this->cp_searchdistricts = $resultsearch['searchdistrict'];
        $this->cp_searchbathroom = $resultsearch['searchbathroom'];
        $this->cp_searchbedroom = $resultsearch['searchbedroom'];
        $this->cp_searchminimum = $resultsearch['searchminimum'];
        $this->cp_searchmaximum = $resultsearch['searchmaximum'];

    }

    public function onSearchHome(){

        $this->page['cp_listsh'] = SaveHome::all();

        $minPrice = trim(str_replace(",","", Input::get('minimum'))) != 'undefined' ? trim(str_replace(",","", Input::get('minimum'))) : '';
        $maxPrice = trim(str_replace(",","", Input::get('maximum'))) != 'undefined' ? trim(str_replace(",","", Input::get('maximum'))) : '';
        $minPrice = empty($minPrice) ? Input::get('minPrice') : $minPrice;
        $maxPrice = empty($maxPrice) ? Input::get('maxPrice') : $maxPrice;

        $resultsearch = $this->FindHome(Input::get('searchtype'),
                                        Input::get('searchdistrict'),
                                        Input::get('searchbathroom'),
                                        Input::get('searchbedroom'),
                                        $minPrice,
                                        $maxPrice);

        $this->page['cp_listho'] = $resultsearch['resulthome'];
        $this->page['auth'] = Session::get('lancaster.member');
        return[
            'searchtype'  => $resultsearch['searchtype'],
            'searchminimum'  => $resultsearch['searchminimum'],
            'searchmaximum'  => $resultsearch['searchmaximum'],
            'searchdistrict' => $resultsearch['searchdistrict'],
            'searchbathroom'  => $resultsearch['searchbathroom'],
            'searchbedroom'  => $resultsearch['searchbedroom'],
        ];
    }

    function FindHome($searchtype,$searchdistrict,$searchbathroom,$searchbedroom,$searchminimum,$searchmaximum){
        $home = new Home;

        if($searchtype != ''){
            $home = $home->where('building_type_home','like','%'. $searchtype .'%')
                        ->orWhere('name_home','like','%'. $searchtype .'%')
                        ->orWhere('description_home','like','%'. $searchtype .'%');
        }else{
            $searchtype = 0;
        }

        if($searchdistrict){
            $home = $home->whereIn('district_home',$searchdistrict);
        }else{
            $searchdistrict = 0;
        }

        if($searchminimum){
            $home = $home->where('listing_price_home','>=',$searchminimum);
        }else{
            $searchminimum = 0;
        }

        if($searchmaximum){
            $home = $home->where('listing_price_home','<=',$searchmaximum);
        }else{
            $searchmaximum = 0;
        }

        if($searchbathroom){
            if($searchbathroom != 5){
                $home = $home->where('baths_home',$searchbathroom);
            }else{
                $home = $home->where('baths_home','>=',$searchbathroom);
            }
        }else{
            $searchbathroom = 0;
        }

        if($searchbedroom){
            if($searchbedroom != 5 && $searchbedroom != '6'){
                $home = $home->where('bedrooms_home',$searchbedroom);
            }elseif($searchbedroom == 5){
                $home = $home->where('bedrooms_home','>=',$searchbedroom);
            }elseif($searchbedroom == 6){
                $home = $home->where('bedrooms_home','Studio');
            }
        }else{
            $searchbedroom = 0;
        }

        $home = $home->orderBy('created_at', 'desc')->get();

        return[
            'searchtype' => $searchtype,
            'searchmaximum' => $searchmaximum,
            'searchminimum' => $searchminimum,
            'searchdistrict' => $searchdistrict,
            'searchbathroom' => $searchbathroom,
            'searchbedroom' => $searchbedroom,
            'resulthome' => $home,
        ];
    }

    public function onSaveHome(){
        if(Members::getAuth()['id']){
            if(post('cp_unsaveho') == 0){
                $cp_sh = new SaveHome;
                $cp_sh->member_id = Members::getAuth()['id'];
                $cp_sh->home_id = post('cp_detailhoid');
                $cp_sh->timestamps = false;
                $cp_sh->save();
            }else{
                SaveHome::where('member_id',Members::getAuth()['id'])->where('home_id',post('cp_detailhoid'))->delete();
            }
            \Flash::success('Successfully!');
            return [
                'success' => true
            ];
        }else{
            \Flash::error('You must login!');
            return [
                'success' => false
            ];
        }

    }

    public function defineProperties()
    {
        return [];
    }

    public $cp_listho;
    public $cp_listsh;
    public $cp_listlcho;
    public $cp_listdtho;
    public $cp_searchtype;
    public $cp_searchbedroom;
    public $cp_searchbathroom;
    public $cp_searchdistricts;
    public $cp_searchminimum;
    public $cp_searchmaximum;
}
