<?php namespace Domdom\Homes;

use Backend;
use System\Classes\PluginBase;

/**
 * Homes Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Home',
            'description' => 'No description provided yet...',
            'author'      => 'Domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'Domdom\Homes\Components\Listho' => 'listho',
            'Domdom\Homes\Components\Detailho' => 'Detailho',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.homes.some_permission' => [
                'tab' => 'Homes',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'homes' => [
                'label'       => 'Homes',
                'url'         => Backend::url('domdom/homes/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.homes.*'],
                'order'       => 500,
            ],
        ];
    }
}
