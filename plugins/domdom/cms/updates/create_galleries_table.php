<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateGalleriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_cms_galleries')){
            Schema::create('domdom_cms_galleries', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->json('video_gallery');
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_galleries');
    }
}
