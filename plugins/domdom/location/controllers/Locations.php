<?php namespace Domdom\Location\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Locations Back-end Controller
 */
class Locations extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'locations');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_location');

        return $this->asExtension('FormController')->create();
    }
}
