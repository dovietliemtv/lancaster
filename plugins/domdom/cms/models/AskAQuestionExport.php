<?php namespace DomDom\Cms\Models;

use Backend\Models\ExportModel;
use DomDom\Cms\Models\AskAQuestion;

class AskAQuestionExport extends ExportModel {
    public function exportData($columns, $sessionKey = null) {
        return AskAQuestion::orderBy('created_at', 'desc')->get()->toArray();
    }
}