<?php 
namespace DomDom\Invest;

use Backend;
use System\Classes\PluginBase;

/**
 * Invest Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'DD Invest',
            'description' => 'DD Invest',
            'author'      => 'DomDom',
            'icon'        => 'icon-usd'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
       //return []; // Remove this line to activate

        return [
            'DomDom\Invest\Components\DDInvest' => 'ddInvest',
            'DomDom\Invest\Components\DDInvestDetail' => 'ddInvestDetail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate
    }
}
