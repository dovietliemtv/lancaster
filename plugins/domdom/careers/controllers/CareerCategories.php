<?php namespace DomDom\Careers\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Career Categories Back-end Controller
 */
class CareerCategories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'careercategories');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_career_category');

        return $this->asExtension('FormController')->create();
    }
}
