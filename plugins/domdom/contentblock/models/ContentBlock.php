<?php namespace Domdom\ContentBlock\Models;

use Model;

/**
 * ContentBlock Model
 */
class ContentBlock extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_contentblock_content_blocks';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['description_contentblock'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
        'code_contentblock' => 'required|unique:domdom_contentblock_content_blocks',
        'description_contentblock' => 'required'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
