<?php namespace Domdom\Homes\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateHomesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_homes_homes')){
            Schema::table('domdom_homes_homes', function(Blueprint $table) {
//            $table->engine = 'InnoDB';
//            $table->increments('id');
//            $table->string('name_home');
//            $table->string('slug_home')->unique();
//            $table->string('property_type_home');
//            $table->string('building_type_home')->unique();
//            $table->string('baths_home');
//            $table->string('bedrooms_home');
//            $table->string('minimum_down_payment_home');
//            $table->string('lot_size_home');
//            $table->string('floor_area_home');
//            $table->string('units_in_building_home');
//            $table->string('district_home');
//            $table->string('building_age_home');
//            $table->json('amenities');
//            $table->bigInteger('listing_price_home');
//            $table->string('downpayment_home');
//            $table->string('interest_rate_home');
//            $table->string('term_home');
//            $table->string('est_monthly_payment_home');

//            $table->integer('location_id_home');
//            $table->string('map_home');
//            $table->text('description_home')->nullable();
//            $table->timestamps();
//            $table->string('type_home');
//            $table->string('building_type_home');

                $table->text('note_home');
            });
        }


    }

    public function down()
    {
        Schema::dropIfExists('domdom_homes_homes');
    }
}
