<?php namespace Domdom\Districtguides\Components;

use Cms\Classes\ComponentBase;
use DomDom\Districtguides\Models\DistrictGuide;
use RainLab\Translate\Classes\Translator;

class Detaildg extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Detail District Guide',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $currLang = Translator::instance()->getLocale();
        $this->cp_detaildg = DistrictGuide::transWhere( 'slug_dg', $this->property('cpslugdg') , $currLang )->first();
    }

    public function defineProperties()
    {
        return [
            'cpslugdg' => [
                'title'             => 'Slug',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_detaildg;
}
