<?php namespace DomDom\Events\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateEventsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_events_events')){
            Schema::create('domdom_events_events', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_event');
                $table->string('slug_event')->unique();
                $table->string('location_event');
                $table->string('map_event');
                $table->text('description_event')->nullable();
                $table->boolean('highlight');
                $table->dateTime('start_time');
                $table->dateTime('end_time');
                $table->timestamps();
            });
        }
        Schema::table('domdom_events_events', function($table)
        {
            $table->boolean('highlight')->after('description_event');
        });


    }

    public function down()
    {
        Schema::dropIfExists('domdom_events_events');
    }
}
