<?php namespace DomDom\DistrictGuides;

use Backend;
use System\Classes\PluginBase;

/**
 * DistrictGuides Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'District Guide',
            'description' => 'No description provided yet...',
            'author'      => 'DomDom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'DomDom\DistrictGuides\Components\Listdg' => 'listdg',
            'DomDom\DistrictGuides\Components\Detaildg' => 'detaildg',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.districtguides.some_permission' => [
                'tab' => 'DistrictGuides',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'districtguides' => [
                'label'       => 'DistrictGuides',
                'url'         => Backend::url('domdom/districtguides/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.districtguides.*'],
                'order'       => 500,
            ],
        ];
    }
}
