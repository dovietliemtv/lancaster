<?php namespace DomDom\DistrictGuides\Models;

use Model;

/**
 * DistrictGuide Model
 */
class DistrictGuide extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_districtguides_district_guides';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name_dg',['slug_dg', 'index' => true],'main_description_dg','description_around_dg','short_description_dg','quote_dg','description_dg','title_feature_dg','name_feature_dg'];

    public $rules = [
        'name_dg' => 'required',
        'slug_dg' => 'required|unique:domdom_districtguides_district_guides',
        'featured_image' => 'required',
        'featured_image_1' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    protected $jsonable = ['feature_dg', 'buttons_custom'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'location' => ['Domdom\Location\Models\Location', 'key' => 'region_dg'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image' => 'System\Models\File',
        'featured_image_1' => 'System\Models\File',
        'featured_image_2' => 'System\Models\File',
    ];
    public $attachMany = [
        'photos' => 'System\Models\File'
    ];
}
