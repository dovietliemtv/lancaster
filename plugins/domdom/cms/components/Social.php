<?php 
namespace DomDom\Cms\Components;

use Cms\Classes\ComponentBase;
use DomDom\Cms\Models\Socials;

class Social extends ComponentBase
{
    public $ddSocialInfo;

    public function componentDetails()
    {
        return [
            'name'        => 'Social Information Component',
            'description' => 'Output social information and more ...'
        ];
    }

    public function onRun()
    {
        $aData['facebook_link'] = Socials::get('facebook');
        $aData['twitter_link'] = Socials::get('twitter');
        $aData['instagram_link'] = Socials::get('instagram');
        $aData['email'] = Socials::get('email');
        $aData['address'] = Socials::get('address');
        $aData['phone'] = Socials::get('phone');
        $aData['footer_description'] = Socials::get('footer_description');
        $aData['privacy_link'] = Socials::get('privacy_link');

        $this->ddSocialInfo = $this->page['ddSocialInfo'] = $aData;
    }

    public function defineProperties()
    {
        return [];
    }
}
