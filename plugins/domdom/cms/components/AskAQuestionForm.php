<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;
use Mail;
use Config;
use Domdom\Cms\Models\Settings;
use Validator;
use ValidationException;
use Request;
use Illuminate\Support\MessageBag;
use DomDom\Cms\Models\AskAQuestion;
use DomDom\Cms\Models\AskAQuestionType;

class AskAQuestionForm extends ComponentBase
{
    /**
     * Contact form validation rules.
     * @var array
     */
    public $formValidationRules = [
        'name' => ['required'],
        'email' => ['required', 'email'],
        'phone' => ['required', 'min:10'],
        'type' => ['required'],
        'message' => ['required'],
    ];


    /**
     * Append custom validation messages. This is used to extend the component
     * with different locales.
     *
     * Example:
     * \Event::listen('cms.component.beforeRunAjaxHandler', function($handler) {
     *     if (get_class($handler) !== 'LaminSanneh\FlexiContact\components\ContactForm') return;
     *     $handler->customMessages = (array) Lang::get('mja.events::validation');
     * });
     *
     * @var array
     */
    public $customMessages = [];

    public function componentDetails()
    {
        return [
            'name'        => 'Ask A Question',
            'description' => 'Ask A Question To Project'
        ];
    }

    public function onRun()
    {
        $this->page['questionTypes'] = AskAQuestionType::where('is_active', 1)->orderBy('name', 'asc')->get();
    }

    public function onAskaquestion(){
        // Build the validator
        $validator = Validator::make(post(), $this->formValidationRules, $this->customMessages);

        // Validate
        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $messSucc = Settings::get('confirmation_text');
        $messUnSucc = Settings::get('confirmation_fail_text');

        $aaq = new AskAQuestion;
        $aaq->name = post('name');
        $aaq->email = post('email');
        $aaq->phone = post('phone');
        $aaq->type = post('type');
        $aaq->message = post('message');

        if($aaq->save()){
            if($this->enableSentMail()){
                Mail::send( 'domdom.cms::emails.message', post(), function($message) {
                    $message->replyTo( post('email'), post('name') )
                        ->to( Settings::get('recipient_email'), Settings::get('recipient_name') )
                        ->subject( Settings::get('subject') );
                });
            }

            \Flash::success($messSucc);
            return [
                'success' => true,
                'message' => $messSucc
            ];
        }else{
            \Flash::error($messUnSucc);
            return [
                'error' => false,
                'message' => $messUnSucc
            ];
        }
    }

    public function defineProperties()
    {
        return [];
    }

    public function enableSentMail(){
        return Settings::get('enable_sent_mail');
    }
}
