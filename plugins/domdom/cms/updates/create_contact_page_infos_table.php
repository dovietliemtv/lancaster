<?php namespace Domdom\Cms\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContactPageInfosTable extends Migration
{
    public function up()
    {
        if ( !Schema::hasTable('domdom_cms_contact_page_infos') ) {
            Schema::create('domdom_cms_contact_page_infos', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->text('description')->nullable();
                $table->string('address')->nullable();
                $table->string('phone')->nullable();
                $table->string('makerting_email')->nullable();
                $table->string('sale_email')->nullable();
                $table->string('other_email')->nullable();
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_cms_contact_page_infos');
    }
}
