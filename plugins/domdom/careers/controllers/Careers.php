<?php namespace DomDom\Careers\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Careers Back-end Controller
 */
class Careers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'careers');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_career');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'type_career' ) {
            return $record->{$columnName} ? 'Part Time' : 'Full Time';
        }
    }
}
