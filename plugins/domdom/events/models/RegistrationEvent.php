<?php namespace Domdom\Events\Models;

use Model;

/**
 * RegistrationEvent Model
 */
class RegistrationEvent extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_events_registration_events';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'event' => ['Domdom\Events\Models\Event', 'key' => 'event_id'],
        'member' => ['Domdom\Membership\Models\Member', 'key' => 'member_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
