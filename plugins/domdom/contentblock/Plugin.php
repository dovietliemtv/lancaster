<?php namespace Domdom\ContentBlock;

use Backend;
use System\Classes\PluginBase;

/**
 * ContentBlock Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Content Block',
            'description' => 'No description provided yet...',
            'author'      => 'Domdom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'Domdom\ContentBlock\Components\Detailcb' => 'detailcb',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.contentblock.some_permission' => [
                'tab' => 'ContentBlock',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'contentblock' => [
                'label'       => 'ContentBlock',
                'url'         => Backend::url('domdom/contentblock/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.contentblock.*'],
                'order'       => 500,
            ],
        ];
    }
}
