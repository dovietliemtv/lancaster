<?php namespace Domdom\Projects\Components;

use Cms\Classes\ComponentBase;
use DomDom\Projects\Models\Project;
use DomDom\Projects\Models\Category;
use DomDom\Projects\Models\ProjectCategory;

class Listpr extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'List Project',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $this->cp_listpr = Project::orderBy('created_at', 'desc')->limit(3)->get();
        $this->cp_otherpr = Project::orderBy('created_at', 'desc')->get();
        $this->cp_listcapr = Category::orderBy('created_at', 'desc')->get();
    }

    public function onFilterProject(){
        $this->page['cp_listpr'] = Project::whereIn( 'id', ProjectCategory::where( 'category_id', post('cp_caprid') )
                                            ->lists( 'project_id' ) )
                                    ->orderBy('created_at', 'desc')->limit(3)->get();
        $this->page['cp_otherpr'] = Project::whereIn( 'id', ProjectCategory::where( 'category_id', post('cp_caprid') )
                                            ->lists( 'project_id' ) )
                                    ->orderBy('created_at', 'desc')->get();
    }

    public function defineProperties()
    {
        return [];
    }

    public $cp_listpr;
    public $cp_listcapr;
    public $cp_otherpr;
}
