<?php namespace Domdom\Events\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateRegistrationEventsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_events_registration_events')){
            Schema::create('domdom_events_registration_events', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('event_id');
                $table->integer('member_id');
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_events_registration_events');
    }
}
