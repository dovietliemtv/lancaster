<?php 
namespace DomDom\Gallery\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDDGalleriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_galleries')){
            Schema::create('domdom_galleries', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->string('slug');
                $table->integer('project_id');
                $table->text('video');
                $table->timestamps();
            });
        }
        Schema::table('domdom_galleries', function($table)
        {
            $table->text('video')->after('project_id');
            $table->dropColumn('video_3d');
            $table->dropColumn('video_prj_news');
            $table->dropColumn('video_other');
        });

    }

    public function down()
    {
        if(Schema::hasTable('domdom_galleries')){
            Schema::dropIfExists('domdom_galleries');
        }
    }
}
