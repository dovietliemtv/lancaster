<?php 
namespace DomDom\Invest\Components;

use Cms\Classes\ComponentBase;
use DomDom\Invest\Models\Invest;

class DDInvestDetail extends ComponentBase
{
    public $ddInvestDetail;

    public function componentDetails()
    {
        return [
            'name'        => 'DD Invest Detail',
            'description' => 'Show DD Invest Detail on the website.'
        ];
    }

    public function onRun(){
        $this->ddInvestDetail  = $this->page['ddInvestDetail'] = Invest::where( 'slug', $this->property('slug') )->first();
    }

    public function defineProperties()
    {
        return [
            'slug' => [
                'title'             => 'Slug',
                'type'              => 'string'
            ]
        ];
    }
}
