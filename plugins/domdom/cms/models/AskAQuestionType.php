<?php namespace DomDom\Cms\Models;

use Model;
use DomDom\Cms\Models\AskAQuestion;

class AskAQuestionType extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $table = 'domdom_projects_askaquestion_type';
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /*
     * Validation
     */
    public $rules = [
        'name' => 'required'
    ];

    /**
     * @var array Attributes that support translation, if available.
     */
    public $translatable = [
        'name',
    ];

    protected $guarded = [];

    public $belongsToMany = [
        'aaq' => ['DomDom\Cms\Models\AskAQuestion',
            'table' => 'domdom_projects_askaquestion',
            'key' => 'type',
            'order' => 'id desc'
        ]
    ];

    public function afterDelete()
    {
        $this->aaq()->detach();
    }

}