<?php namespace Domdom\Cms\Components;

use Cms\Classes\ComponentBase;

class Aboutpage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'About Page Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {

    }

    public function defineProperties()
    {
        return [];
    }
}
