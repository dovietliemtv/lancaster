<?php namespace DomDom\Careers\Models;

use Model;

/**
 * CareerCategory Model
 */
class CareerCategory extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_careers_career_categories';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name_category_career','slug_category_career'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
        'name_category_career' => 'required',
        'slug_category_career' => 'required|unique:domdom_careers_career_categories'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
