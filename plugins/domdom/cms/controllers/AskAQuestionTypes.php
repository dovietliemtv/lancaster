<?php namespace DomDom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DomDom\Cms\Models\AskAQuestionType;
use Flash;

class AskAQuestionTypes extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['domdom.cms.access_askaquestiontype'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('DomDom.Cms', 'cms', 'askaquestiontypes');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_askaquestiontypes');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'is_active' ) {
            return $record->is_active == 1 ? 'Active' : 'Disable';
        }
    }

}