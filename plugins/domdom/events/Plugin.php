<?php namespace DomDom\Events;

use Backend;
use System\Classes\PluginBase;

/**
 * Events Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Event',
            'description' => 'No description provided yet...',
            'author'      => 'DomDom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'DomDom\Events\Components\Listev' => 'listev',
            'DomDom\Events\Components\Detailev' => 'detailev',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.events.some_permission' => [
                'tab' => 'Events',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'events' => [
                'label'       => 'Events',
                'url'         => Backend::url('domdom/events/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.events.*'],
                'order'       => 500,
            ],
        ];
    }
}
