<?php namespace DomDom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'categories');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_category');

        return $this->asExtension('FormController')->create();
    }
}
