<?php 
namespace DomDom\Invest\Models;

use Model;

/**
 * NewsPage Model
 */
class Invest extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title','description'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_invests';

    public $rules = [
        'title' => 'required',
        'description' => 'required',
        'featured_image' => 'required',
        'slug' => 'required|unique:domdom_invests',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'category' => ['DomDom\Invest\Models\Category', 'key' => 'category_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image' => 'System\Models\File',
    ];
}
