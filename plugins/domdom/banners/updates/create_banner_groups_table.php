<?php namespace DomDom\Banners\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBannerGroupsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_banners_banner_groups')){
            Schema::create('domdom_banners_banner_groups', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('banner_id')->unsigned();
                $table->integer('group_id')->unsigned();
                $table->primary(['banner_id', 'group_id']);
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_banners_banner_groups');
    }
}
