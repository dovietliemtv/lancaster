<?php namespace DomDom\Cms\Models;

use Model;

/**
 * NewsPage Model
 */
class NewsPage extends Model
{
    use \October\Rain\Database\Traits\Validation;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['title','description'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_cms_news_pages';

    public $rules = [
        'title' => 'required',
        'description' => 'required',
        'featured_image' => 'required',
        'slug' => 'required|unique:domdom_cms_news_pages',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'category' => ['DomDom\Cms\Models\Category', 'key' => 'category_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image' => 'System\Models\File',
    ];
    public $attachMany = [
        'gallery' => 'System\Models\File'
    ];
}
