<?php namespace Domdom\Events\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Registration Events Back-end Controller
 */
class RegistrationEvents extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'registrationevents');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_registrationevent');

        return $this->asExtension('FormController')->create();
    }
}
