<?php namespace DomDom\Banners\Models;

use Model;

/**
 * Group Model
 */
class Group extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_banners_groups';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
        'code_group' => 'required|unique:domdom_banners_groups'
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'banners' => [
            'Domdom\Banners\Models\Banner',
            'table'    => 'domdom_banners_banner_groups',
            'key' => 'group_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
