<?php namespace Domdom\Banners\Components;

use Cms\Classes\ComponentBase;
use DomDom\Banners\Models\Banner;

class Bannerpage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Banner Page',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $this->cp_bannerpage = Banner::where('code_banner', $this->property('codebannerpage'))->first();
    }

    public function defineProperties()
    {
        return [
            'codebannerpage' => [
                'title'             => 'Code',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_bannerpage;
}
