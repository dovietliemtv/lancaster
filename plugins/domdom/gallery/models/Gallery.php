<?php 
namespace DomDom\Gallery\Models;

use Model;

/**
 * Gallery Model
 */
class Gallery extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_galleries';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];
    protected $jsonable = ['video'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'project' => ['DomDom\Projects\Models\Project', 'key' => 'project_id'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image' => 'System\Models\File',
        'attach_file' => 'System\Models\File',
    ];
    public $attachMany = [
        'gallery_image' => 'System\Models\File',
    ];
}
