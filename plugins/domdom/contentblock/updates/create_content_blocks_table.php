<?php namespace Domdom\ContentBlock\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateContentBlocksTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_contentblock_content_blocks')){
            Schema::create('domdom_contentblock_content_blocks', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('code_contentblock');
                $table->text('description_contentblock')->nullable();
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_contentblock_content_blocks');
    }
}
