<?php namespace DomDom\Cms;

use Backend;
use System\Classes\PluginBase;
use BackendMenu;

/**
 * Cms Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Cms',
            'description' => 'No description provided yet...',
            'author'      => 'DomDom',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        BackendMenu::registerContextSidenavPartial('DomDom.Cms', 'cms', 'plugins/domdom/cms/partials/sidebar');
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Ask A Question Settings',
                'description' => 'Manage the settings for the ask a question form in project detail',
                'category'    => 'Marketing',
                'icon'        => 'icon-envelope',
                'class'       => 'DomDom\Cms\Models\Settings',
                'permissions' => ['domdom.cms.access_settings'],
                'order'       => 101
            ],
            'socials' => [
                'label'       => 'Ganeral Option',
                'description' => 'Manage the settings for the social link on website',
                'category'    => 'Marketing',
                'icon'        => 'icon-cog',
                'class'       => 'DomDom\Cms\Models\Socials',
                'permissions' => ['domdom.cms.access_settings'],
                'order'       => 101
            ],
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
//        return []; // Remove this line to activate

        return [
            'DomDom\Cms\Components\Homepage' => 'homepage',
            'DomDom\Cms\Components\Aboutpage' => 'aboutpage',
            'DomDom\Cms\Components\Contactpage' => 'contactpage',
            'DomDom\Cms\Components\AskAQuestionForm' => 'askaquestion',
            'DomDom\Cms\Components\CpGallery' => 'cpgallery',
            'DomDom\Cms\Components\CpNews' => 'cpnews',
            'DomDom\Cms\Components\CpSinglePost' => 'cpsinglepost',
            'DomDom\Cms\Components\Social' => 'ddSocialInfo',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'domdom.cms.some_permission' => [
                'tab' => 'Cms',
                'label' => 'Some permission'
            ],
            'domdom.cms.access_askaquestiontype' => [
                'tab' => 'Projects',
                'label' => 'New Question Type'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
//        return []; // Remove this line to activate

        return [
            'cms' => [
                'label'       => 'Content',
                'url'         => Backend::url('domdom/projects/projects'),
                'icon'        => 'icon-leaf',
                'permissions' => ['domdom.cms.*'],
                'order'       => 500,

                'sideMenu' => [
                    'projects' => [
                        'label'       => 'Projects',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/projects/projects'),
                        'group'       => 'Projects',

                    ],

                    'new_project' => [
                        'label'       => 'New Project',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/projects/projects/create'),
                        'group'       => 'Projects',

                    ],

                    'categories' => [
                        'label'       => 'Categories',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/projects/categories'),
                        'group'       => 'Projects',

                    ],

                    'new_category' => [
                        'label'       => 'New Category',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/projects/categories/create'),
                        'group'       => 'Projects',

                    ],


                    'askaquestiontypes' => [
                        'label'       => 'Question Type',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/cms/askaquestiontypes'),
                        'group'       => 'Projects',

                    ],

                    'new_askaquestiontypes' => [
                        'label'       => 'New Question Type',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/cms/askaquestiontypes/create'),
                        'group'       => 'Projects',
                    ],

                    'askaquestions' => [
                        'label'       => 'Question Manage',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/cms/askaquestions'),
                        'group'       => 'Projects',

                    ],

                    'applycareers' => [
                        'label'       => 'List Apply',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/careers/applycareers'),
                        'group'       => 'Careers',

                    ],

                    'careers' => [
                        'label'       => 'Careers',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/careers/careers'),
                        'group'       => 'Careers',

                    ],

                    'new_career' => [
                        'label'       => 'New Career',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/careers/careers/create'),
                        'group'       => 'Careers',

                    ],

                    'careercategories' => [
                        'label'       => 'Career Categories',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/careers/careercategories'),
                        'group'       => 'Careers',

                    ],

                    'new_career_category' => [
                        'label'       => 'New Career Categories',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/careers/careercategories/create'),
                        'group'       => 'Careers',

                    ],

                    'events' => [
                        'label'       => 'Events',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/events/events'),
                        'group'       => 'Events',

                    ],

                    'new_event' => [
                        'label'       => 'New Event',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/events/events/create'),
                        'group'       => 'Events',

                    ],

                    'registrationevents' => [
                        'label'       => 'List Registration',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/events/registrationevents'),
                        'group'       => 'Events',

                    ],

                    'categories' => [
                        'label'       => 'Categories',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/cms/categories'),
                        'group'       => 'News',

                    ],

                    'new_category' => [
                        'label'       => 'Create Category',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/cms/categories/create'),
                        'group'       => 'News',

                    ],

                    'newspages' => [
                        'label'       => 'News',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/cms/newspages'),
                        'group'       => 'News',

                    ],

                    'new_newspage' => [
                        'label'       => 'Create News',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/cms/newspages/create'),
                        'group'       => 'News',

                    ],

                    'districtguides' => [
                        'label'       => 'District Guides',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/districtguides/districtguides'),
                        'group'       => 'District Guides',

                    ],

                    'new_districtguide' => [
                        'label'       => 'New District Guide',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/districtguides/districtguides/create'),
                        'group'       => 'District Guides',

                    ],

                    'invest_cates' => [
                        'label'       => 'Manage Categories',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/invest/categories'),
                        'group'       => 'How to Invest',

                    ],

                    'new_invest_cate' => [
                        'label'       => 'New Invest Category',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/invest/categories/create'),
                        'group'       => 'How to Invest',

                    ],

                    'invests' => [
                        'label'       => 'Manage Invests',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/invest/invests'),
                        'group'       => 'How to Invest',

                    ],

                    'new_invest' => [
                        'label'       => 'New Invest',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/invest/invests/create'),
                        'group'       => 'How to Invest',

                    ],

                    'locations' => [
                        'label'       => 'List Location',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/location/locations'),
                        'group'       => 'Location',

                    ],

                    'new_location' => [
                        'label'       => 'New Location',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/location/locations/create'),
                        'group'       => 'Location',

                    ],

                    'districts' => [
                        'label'       => 'List District',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/location/districts'),
                        'group'       => 'Location',

                    ],

                    'new_district' => [
                        'label'       => 'New District',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/location/districts/create'),
                        'group'       => 'Location',

                    ],


                    'banners' => [
                        'label'       => 'Banners',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/banners/banners'),
                        'group'       => 'Banners',

                    ],

                    'new_banner' => [
                        'label'       => 'New Banner',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/banners/banners/create'),
                        'group'       => 'Banners',

                    ],

                    'groups' => [
                        'label'       => 'List Slider',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/banners/groups'),
                        'group'       => 'Banners',

                    ],

                    'new_group' => [
                        'label'       => 'New Slider',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/banners/groups/create'),
                        'group'       => 'Banners',

                    ],

                    'contentblocks' => [
                        'label'       => 'List Content Block',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/contentblock/contentblocks'),
                        'group'       => 'Content Block',

                    ],

                    'new_contentblock' => [
                        'label'       => 'New Content Block',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/contentblock/contentblocks/create'),
                        'group'       => 'Content Block',

                    ],

                    'members' => [
                        'label'       => 'List Member',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/membership/members'),
                        'group'       => 'Member',

                    ],

                    'new_member' => [
                        'label'       => 'New Member',
                        'icon'        => 'icon-plus-circle',
                        'url'         => Backend::url('domdom/membership/members/create'),
                        'group'       => 'Member',

                    ],
                    'library_list' => [
                        'label'       => 'Manage Album',
                        'icon'        => 'icon-list',
                        'url'         => Backend::url('domdom/gallery/galleries'),
                        'group'       => 'Library',
                    ],
                    'new_library' => [
                        'label'       => 'New Album',
                        'icon'        => 'icon-file-image-o',
                        'url'         => Backend::url('domdom/gallery/galleries/create'),
                        'group'       => 'Library',
                    ],

                    //Contact
                    'contact_info' => [
                        'label'       => 'Contact Information',
                        'icon'        => 'icon-user',
                        'url'         => Backend::url('domdom/cms/contactpageinfo/update/1'),
                        'group'       => 'Contacts',

                    ],
                    'contacts' => [
                        'label'       => 'Manage contacts',
                        'icon'        => 'icon-envelope',
                        'url'         => Backend::url('laminsanneh/flexicontact/contacts'),
                        'group'       => 'Contacts',

                    ],
                ]
            ],
        ];
    }

    public function registerMailTemplates()
    {
        return [
            'domdom.cms::emails.message' => 'Email sent to the administrator after user filled out the ask a question form',
        ];
    }
}
