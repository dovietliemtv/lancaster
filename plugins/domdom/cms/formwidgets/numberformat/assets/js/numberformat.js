accounting.settings = {
    currency: {
        symbol : "",   // default currency symbol is '$'
        format: "%v", // controls output: %s = symbol, %v = value/number (can be object: see below)
        decimal : ".",  // decimal point separator
        thousand: ",",  // thousands separator
        precision : 0   // decimal places
    },
    number: {
        precision : 0,  // default precision on numbers is 0
        thousand: ",",
        decimal : "."
    }
};
(function($) {
    $(document).ready(function() {
        $('.money-format').on('keyup', function() {
            this.value = accounting.formatMoney( this.value );
        });
    })
})(jQuery);