<?php
namespace DomDom\Cms\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTablesAskQuestionType extends Migration {

    public function up() {
        if ( !Schema::hasTable('domdom_projects_askaquestion_type') ) {
            Schema::create('domdom_projects_askaquestion_type', function($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->smallInteger('is_active');
                $table->timestamps();
            });
        }
    }
    public function down() {
        Schema::dropIfExists('domdom_projects_askaquestion_type');
    }

}