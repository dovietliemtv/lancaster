<?php 
namespace Domdom\Invest\Components;

use Cms\Classes\ComponentBase;
use DomDom\Invest\Models\Invest;
use DomDom\Invest\Models\Category;
use Input;

class DDInvest extends ComponentBase
{
    public $ddInvest;
    public $ddCategories;

    public function componentDetails()
    {
        return [
            'name'        => 'DD Invest Component',
            'description' => 'Show DD Invest on the website'
        ];
    }

    public function onRun(){
        $this->page['ddInvest'] = Invest::orderBy('created_at', 'desc')->get();
        $this->ddCategories = $this->page['ddCategories'] = Category::orderBy('title', 'asc')->get();
    }

    public function onFilterInvest(){

        if(Input::get('category') != 0){
            $ddInvest = Invest::where('category_id',Input::get('category'))->orderBy('created_at', 'desc')->get();
        }else{
            $ddInvest = Invest::orderBy('created_at', 'desc')->get();
        }
        $this->page['ddInvest'] = $ddInvest;
    }

    public function defineProperties()
    {
        return [];
    }
}
