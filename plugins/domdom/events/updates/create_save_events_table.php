<?php namespace DomDom\Events\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSaveEventsTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_events_save_events')){
            Schema::create('domdom_events_save_events', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('event_id')->unsigned();
                $table->integer('member_id')->unsigned();
                $table->primary(['event_id', 'member_id']);
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_events_save_events');
    }
}
