<?php namespace DomDom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use DomDom\Cms\Models\AskAQuestion;
use Flash;

class AskAQuestions extends Controller
{
    public $implement = [
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ImportExportController'
    ];

    public $listConfig = 'config_list.yaml';
    public $importExportConfig = 'config_import_export.yaml';

    public $requiredPermissions = ['domdom.cms.access_askaquestions'];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('DomDom.Cms', 'cms', 'askaquestions');
    }

}