<?php namespace DomDom\Banners\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateBannersTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_banners_banners')){
            Schema::table('domdom_banners_banners', function(Blueprint $table) {
//            $table->engine = 'InnoDB';
//            $table->increments('id');
//            $table->string('code_banner');
//            $table->string('title_banner');
//            $table->timestamps();
                $table->string('link_banner')->nullable();
                $table->text('description_banner')->nullable();
                $table->text('quote_banner')->nullable();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_banners_banners');
    }
}
