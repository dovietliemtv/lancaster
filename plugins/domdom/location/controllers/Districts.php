<?php namespace Domdom\Location\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Districts Back-end Controller
 */
class Districts extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'districts');
    }
    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_district');

        return $this->asExtension('FormController')->create();
    }
}
