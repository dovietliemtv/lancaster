<?php namespace LaminSanneh\FlexiContact\Models;

use Backend\Models\ExportModel;
use LaminSanneh\FlexiContact\Models\Contact;

class ContactExport extends ExportModel {
    public function exportData($columns, $sessionKey = null) {
        return Contact::orderBy('type', 'asc')->get()->toArray();
    }
}