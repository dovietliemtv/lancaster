<?php namespace Domdom\Membership\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Members Back-end Controller
 */
class Members extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'members');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_member');

        return $this->asExtension('FormController')->create();
    }
}
