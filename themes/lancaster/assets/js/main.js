
"use strict";

(function($) {
    $(document).ready(function () {

        Init();


        // check input happy
        function minLength(val, min) {
            return val.length < min ? new Error('Your name must be longer than ' + min + ' character' + (min !== 0 ? 's' : '') + '.') : true;
        }

        function maxLength(val, max) {
            return val.length > max ? new Error('Your name must be shorter than ' + max + ' character' + (max !== 0 ? 's' : '') + '.') : true;
        }
        $('#loginForm').isHappy({
            fields: {
                '#signin-email': {
                    required: true,
                    test: happy.email
                },
                '#signin-passwd': {
                    required: true,
                    test: [minLength, maxLength],
                    arg: [6, 64]
                }
            }
        });
        $('#registerForm').isHappy({
            fields: {
                '#signup-firstname': {
                    required: true
                },
                '#signup-lastname': {
                    required: true
                },
                '#signup-email': {
                    required: true,
                    test: happy.email
                },
                '#signup-passwd': {
                    required: true
                },
                '#signup-re-passwd': {
                    required: true
                },
                '#signup-phone': {
                    required: true,
                    test: happy.phone

                }
            }
        });

        $('#contactForm').isHappy({
            fields: {
                '#yourname': {
                    required: true
                },
                '#youremail': {
                    required: true,
                    test: happy.email

                },
                '#message': {
                    required: true
                }
            }
        });
        $('#askAQuestionForm').isHappy({
            fields: {
                '#name_aaq': {
                    required: true
                },
                '#email_aaq': {
                    required: true,
                    test: happy.email

                },
                '#phone_aaq': {
                    required: true,
                    test: happy.phone
                },
                '#help_aaq':{
                    required: true,
                    test: function(value) {
                        if(value == 0) {
                            return false;
                        }else {
                            return true;
                        }
                    }
                },
                '#message_aaq': {
                    required: true
                }
            }
        });


        $('#applyForm').isHappy({
            fields: {
                '#name_pfl': {
                    required: true
                },
                '#email_pfl': {
                    required: true,
                    test: happy.email

                },
                '#file_pfl': {
                    required: true
                },
                '#message_pfl': {
                    required: true
                }
            }
        });






    }); //END DOCUMENT READY

    function Init() {
        var body = $('body, html');

        initOtherCasesSlider();
        setMaxheight();
        $('.banner-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<div class="next"></div>',
            prevArrow: '<div class="pre"></div>'
        });
        $('.feature-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            nextArrow: '<div class="next"><span>Next Project</span></div>',
            prevArrow: '<div class="pre"><span>Previous Project</span></div>',
            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        dots: true
                    }
                }
            ]
        });
        var countSlide = $(".feature-slider").slick("getSlick").slideCount;
        if(countSlide >=5){
            $('.feature-slider.slick-dotted .slick-dots').css({
                'bottom': '-80px'
            });
            $('.feature-slider.slick-dotted.slick-slider').css({
                'margin-bottom': '80px'
            });
        }



        if($('html').hasClass('mobile')){
            $('.upcomming-post-slider').slick({
                dots: true,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                arrows: false
            });
            var countSlide = $(".upcomming-post-slider").slick("getSlick").slideCount;
            if(countSlide >=5){
                $('.upcomming-post-slider.slick-dotted .slick-dots').css({
                    'bottom': '-80px'
                });
                $('.upcomming-post-slider.slick-dotted.slick-slider').css({
                    'margin-bottom': '80px'
                });
            }
        }

        if($('html').hasClass('mobile')){
            $('.project-slider').slick({
                dots: false,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                arrows: false,
                responsive: [
                    {
                        breakpoint: 800,
                        settings: {
                            dots: true
                        }
                    }
                ]
            });
        }

        if($('html').hasClass('mobile')){

            // var inst = $('[data-remodal-id=fillter]').remodal();
            // inst.open();
            // $('.fillter').remodal({ hashTracking: false });
            $('.filter-mobile a').on('click', function () {
                if(!$('.filter').hasClass('remodal')){
                    $('.filter').addClass('remodal');
                    var inst = $('[data-remodal-id=filter]').remodal();
                    inst.open();
                }
            });
            // var modalSuccess = $.remodal.lookup[$('[data-remodal-id=filter]').data('remodal')];
            // modalSuccess.open();
        }

        if($('html').hasClass('mobile')){
            $('.gallery-slider').slick({
                dots: false,
                infinite: true,
                speed: 500,
                fade: true,
                cssEase: 'linear',
                arrows: false,
                responsive: [
                    {
                        breakpoint: 800,
                        settings: {
                            dots: true
                        }
                    }
                ]
            });
            var countSlide = $(".gallery-slider").slick("getSlick").slideCount;
            if(countSlide >=5){
                $('.slick-dotted .slick-dots').css({
                    'bottom': '-80px'
                });
                $('.slick-dotted.slick-slider').css({
                    'margin-bottom': '80px'
                });
            }
        }
        $('.buy-rent-slider').slick({
            dots: false,
            infinite: false,
            arrows: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoPlaySpeed: 6000,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 1080,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 3,
                        dots: true,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 620,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        slidesToScroll: 2

                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        slidesToScroll: 1

                    }
                }
            ]
            // variableWidth: true,
        });
        $('.customer-thinks-mobile').slick({
            dots: false,
            infinite: false,
            slidesToShow: 5,
            slidesToScroll: 1,
            autoPlaySpeed: 6000,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 1080,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 3,
                        dots: true,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 620,
                    settings: {
                        slidesToShow: 2,
                        dots: true,
                        slidesToScroll: 2

                    }
                },
                {
                    breakpoint: 500,
                    settings: {
                        slidesToShow: 1,
                        dots: true,
                        slidesToScroll: 1

                    }
                }
            ]
            // variableWidth: true,
        });
        $('.customer-slider').slick({
            dots: false,
            infinite: true,
            speed: 300,
            slidesToShow: 7,
            centerMode: true,
            variableWidth: true,
            autoPlay: true,
            prevArrow:"<div class='arrow arrow-pre'><span></span></div>",
            nextArrow:"<div class='arrow arrow-next'><span></span></div>",

            responsive: [
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        dots: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: false
                    }
                }
            ]
        });
        var countSlide = $(".customer-slider").slick("getSlick").slideCount;
        if(countSlide >=5){
            $('.customer-slider.slick-dotted .slick-dots').css({
                'bottom': '-80px'
            });
            $('.customer-slider.slick-dotted.slick-slider').css({
                'margin-bottom': '80px'
            });
        }


        $('.our-story-slider').slick({
            dots: false,
            infinite: true,
            speed: 500,
            arrows: false,
            fade: true,
            cssEase: 'linear'
        });
        $('.find-your-home-detail-slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 3,
            variableWidth: false,
            responsive: [
                {
                    breakpoint: 1080,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 780,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        speed: 500,
                        fade: true,
                        cssEase: 'linear',
                        arrows: false
                    }
                }
            ]

        });
        $('a.show-menu').on("click",function (e) {
            e.preventDefault();
            $('.menu-wrap').addClass('menu_active');
            $('.menu-wrap').fadeIn(700);
            $('html').css({
                'overflow': 'hidden'
            })

        });
        $('.minus-menu').on("click",function (e) {
            e.preventDefault();
            $('.menu-wrap').fadeOut('fast');
            $('.menu-wrap').removeClass('menu_active');
            $('html').css({
                'overflow': 'visible'
            });
        });
        $(document).mouseup(function(e)
        {
            var container = $(".menu-wrap");
            if (!container.is(e.target) && container.has(e.target).length === 0)
            {
                container.fadeOut('fast');
            }
            var langBox = $(".lang ul");
            if (!langBox.is(e.target) && langBox.has(e.target).length === 0)
            {
                $('.lang ul').fadeOut('fast');
                $('html').css({
                    'overflow': 'visible'
                })
            }
        });

        $('.minus-icon-popup').on('click',function () {
            $('.registration-popup-wrap').fadeOut('fast');
            $('.menu-wrap').fadeOut('fast');
        });
        $('.go-top-btn-wrap a').on('click', function (e) {
            e.preventDefault();
            var body = $("html, body");
            body.stop().animate({scrollTop:0}, 500, 'swing');
        });
        // registration forms
        $('.login-btn').on('click', function () {
            $('.menu-wrap').fadeOut('fast');
        });
        $('.register-btn').on('click', function () {
            $('.menu-wrap').fadeOut('fast');
        });

        $('.nav-custom li').on('click',function () {
            $('.nav-custom li').removeClass('active');
            $(this).addClass('active');
        });

        $('.current-lang').on('click', function () {
            if($('.lang ul:visible').length == 0)
            {
                $('.lang ul').fadeIn('fast');
            }else{
                $('.lang ul').fadeOut('fast');
            }
        });

        body.waitForImages(function() {
            setTimeout(function () {
                $('.loadding-wrap').fadeOut();
            },1000);
        });




        $('.btn-ask').on('click', function (e) {
            e.preventDefault();
            $('body, html').stop().animate({ scrollTop: $('.ask-a-question').position().top}, 500, 'swing');


        });
        $('.home-img').magnificPopup({
            delegate: 'a.img-zoom',
            type: 'image'
        });

        $('.btn-ask-cal').on('click', function (e) {
            e.preventDefault();
            if ($('.calendar-options:visible').length == 0) {
                $('.calendar-options').slideDown();
                $('.btn-ask-cal').addClass('active');
            } else {
                $('.calendar-options').slideUp();
                $('.btn-ask-cal').removeClass('active');

            }
        });

        if($('html').hasClass('mobile')){
            $('.nav-custom-active').on('click', function () {
                if($('.career-filter .nav-custom:visible').length == 0){
                    $('.career-filter .nav-custom').slideDown();
                }else {
                    $('.career-filter .nav-custom').slideUp();
                }
            });

            $('.nav-custom li').on('click', function () {
                var text = $(this).find('a span').text();
                console.log(text);
                $('.nav-custom-active').html(text);
                $('.nav-custom').hide();
            })
        }

        rangeSlider('dd-min-price');
        rangeSlider('dd-max-price');



        // input type file js
        $('#chooseFile').bind('change', function () {
            var filename = $("#chooseFile").val();
            if (/^\s*$/.test(filename)) {
                $(".file-upload").removeClass('active');
                $("#noFile").text("No file chosen...");
            }
            else {
                $(".file-upload").addClass('active');
                $("#noFile").text(filename.replace("C:\\fakepath\\", ""));
            }
        });

        // slider gallery

        $('.dd-b-gallery-slider').slick({
            dots: true,
            arrows: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 768,
                    settings: "unslick"
                }
            ]
        });

    // slider highlight news
        $('.dd-b-news-highlight').slick({
            dots: false,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            prevArrow:"<div class='arrow-slide arrow-left'><span></span></div>",
            nextArrow:"<div class='arrow-slide arrow-right'><span></span></div>"

        });

        //popup for video
        $('.dd-iframe').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'iframe',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });

        $('.gallery-item-detail').magnificPopup({
            delegate: 'a', // child items selector, by clicking on it popup will open
            type: 'image',

            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0,1] // Will preload 0 - before current, and 1 after the current image
            },
            zoom: {
                enabled: true,
                duration: 300, // duration of the effect, in milliseconds
                easing: 'ease-in-out' // CSS transition
            }
        });

        // gallery tab

        $('#gallery-select').on('change', function () {
            if($(this).val() == 1){
                $('.dd-b-gallery-slider').removeClass('active');
                $('.dd-image').addClass('active');

            }else if($(this).val() == 2){
                $('.dd-b-gallery-slider').removeClass('active');
                $('.dd-video').addClass('active');
            }else{
                $('.dd-b-gallery-slider').removeClass('active');
                $('.dd-e-brochure').addClass('active');

            }
        });
        // set height title and excerpt news
        var maxHeightNewsTitle = -1;
        var maxHeightNewsExcerpt = -1;
        $(".dd-news-item .dd-body h4").each(function() {
            var h = $(this).height();
            maxHeightNewsTitle = h > maxHeightNewsTitle ? h : maxHeightNewsTitle;
        });
        $(".dd-news-item .dd-body .dd-excerpt").each(function() {
            var h = $(this).height();
            maxHeightNewsExcerpt = h > maxHeightNewsExcerpt ? h : maxHeightNewsExcerpt;
        });
        $('.dd-news-item .dd-body h4').css({'height' : maxHeightNewsTitle + 'px'});
        $('.dd-news-item .dd-body .dd-excerpt').css({'height' : maxHeightNewsExcerpt + 'px'});



        $('.js-vacanies').on('click', function (e) {
            e.preventDefault();
            $('body, html').stop().animate({ scrollTop: $('.working-main-content').position().top}, 500, 'swing');

        });



    } // end init
    rangeSlider = function($id){
        var slider = $('#'+ $id +'.range-slider'),
            range = $('#'+ $id +' .range-slider__range'),
            value = $('#'+ $id +' .range-slider__value');
        slider.each(function(){
            value.each(function(){
                var value = $(this).prev().attr('value');
                $('#'+ $id).val(value);
                $(this).html(value);
            });
            range.on('input', function(){
                var moneyVal =  this.value.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
                value.html(moneyVal);
            });
        });
    };

    $(window).on('scroll', function(){
        var currenPos = $(this).scrollTop();
        if(currenPos >= 100){
            $('header').addClass('sticky');

            $('.go-top-btn-wrap').addClass('show');
        }else{
            $('header').removeClass('sticky');
            $('.go-top-btn-wrap').removeClass('show');

        }
    });

})(jQuery);


// auto click when onchange rangeslider
var minPrive;
var maxPrice;
$('#dd-minPrice, #dd-maxPrice').on('change', function () {
    minPrive = $('#dd-minPrice').val();
    maxPrice = $('#dd-maxPrice').val();
});
function url_change(){
    var url = '';
    var val1 = $('#result_type').data('valtypehome');
    var val2 = $('#result_district').data('valdistricthome');
    var val3 = minPrive;
    var val4 = maxPrice;
    var val5 = $('#result_bedroom').data('valbedroomhome');
    var val6 = $('#result_bathroom').data('valbathroomhome');

    if( val1 != 0){
        url = url + '&type=' + val1;
    }
    if( val2 != 0){
        url = url + '&district=' + val2;
    }
    if( val3 != 0){
        url = url + '&minimum=' + val3;
    }
    if( val4 != 0){
        url = url + '&maximum=' + val4;
    }

    if( val5 != 0){
        url = url + '&bedroom=' + val5;
    }
    if( val6 != 0){
        url = url + '&bathroom=' + val6;
    }

    url = window.location.href.split('?')[0] + '?' + url.substr(1);
    history.replaceState(undefined, '', url);

}
var initOtherCasesSlider = function () {
    var  otherSlider = $('.other-cases-slider');

    if($('.other-cases-slider.slick-initialized').length) {
        otherSlider.slick('unslick');
    }

    otherSlider.slick({
        dots: true,
        idots: true,
        infinite: false,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: false,
        autoplaySpeed: 6000,
        cssEase: 'linear',
        responsive: [
            {
                breakpoint: 1080,
                settings: {
                    slidesToShow: 3,
                    dots: true
                }
            },
            {
                breakpoint: 780,
                settings: {
                    slidesToShow: 1,
                    dots: true

                }
            }

        ]
        // variableWidth: true,
    });
};
function transferdataworkapply() {
    $(".transfer_data_work_apply").click(function() {
        $('#name_work_pfl').val($(this).data('valworkapply'));
        console.log($('#name_work_pfl').val());
    });
}

transferdataworkapply();

var numberformatjs =  $('#result_price_home').data('valpricehome');
if(typeof numberformatjs != 'undefined'){
    $('.numberjs').text(parseFloat(numberformatjs.replace(/,/g , ".")));
}

function delayreload() {
    setTimeout(function () {
        location.reload();
    },1000)
}

$(".formattedNumberField").on('keyup', function(){
    if($(this).val() != ''){
        var n = parseInt($(this).val().replace(/\D/g,''),10);
        $(this).val(n.toLocaleString());
    }
});

function setMaxheight() {
    // max hieght
    var maxHeight = -1;
    $('.house-info').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    $('.house-info').each(function() {
        $(this).height(maxHeight);
    });
    $('.district-guides-info').each(function() {
        maxHeight = maxHeight > $(this).height() ? maxHeight : $(this).height();
    });
    $('.district-guides-info').each(function() {
        $(this).height(maxHeight);
    });

}