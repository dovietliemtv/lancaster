<?php
namespace LaminSanneh\FlexiContact\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateAllTables extends Migration {

    public function up() {
        if ( !Schema::hasTable('dd_contacts') ) {
            Schema::create('dd_contacts', function($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name');
                $table->string('email');
                $table->string('phone')->nullable();
                $table->text('request')->nullable();
                $table->timestamps();
            });
        }
    }
    public function down() {
        if ( Schema::hasTable('dd_contacts') ) {
            Schema::dropIfExists('dd_contacts');
        }
    }

}