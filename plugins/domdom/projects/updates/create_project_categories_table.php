<?php namespace DomDom\Projects\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProjectCategoriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_projects_project_categories')){
            Schema::create('domdom_projects_project_categories', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('project_id')->unsigned();
                $table->integer('category_id')->unsigned();
                $table->primary(['project_id', 'category_id']);
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_projects_project_categories');
    }
}
