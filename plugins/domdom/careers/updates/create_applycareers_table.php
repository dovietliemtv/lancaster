<?php namespace DomDom\Careers\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateApplycareersTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_careers_applycareers')){
            Schema::create('domdom_careers_applycareers', function(Blueprint $table) {
               $table->engine = 'InnoDB';
               $table->increments('id');
               $table->string('name_apply');
               $table->string('email_apply');
               $table->text('message_apply')->nullable();
               $table->string('work_apply')->nullable();
               $table->string('file_url')->nullable();
               $table->string('file_name')->nullable();
               $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_careers_applycareers');
    }
}
