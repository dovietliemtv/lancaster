<?php namespace DomDom\Projects\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('domdom_projects_categories')){
            Schema::create('domdom_projects_categories', function(Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('name_category_pj');
                $table->string('slug_category_pj')->unique();
                $table->timestamps();
            });
        }

    }

    public function down()
    {
        Schema::dropIfExists('domdom_projects_categories');
    }
}
