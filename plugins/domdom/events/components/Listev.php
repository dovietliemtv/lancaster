<?php namespace Domdom\Events\Components;

use Cms\Classes\ComponentBase;
use DomDom\Events\Models\Event;

class Listev extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'List Event',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun(){
        $this->cp_listev = Event::orderBy('created_at', 'desc')->get();
        $this->cp_listevupcomming = Event::orderBy('created_at', 'desc')->get();
    }

    public function defineProperties()
    {
        return [];
    }

    public $cp_listev;
    public $cp_listevupcomming;
}
