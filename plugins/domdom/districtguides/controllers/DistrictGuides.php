<?php namespace DomDom\DistrictGuides\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * District Guides Back-end Controller
 */
class DistrictGuides extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'districtguides');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_districtguide');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image' ) {
            return '<img src="' . $record->featured_image->path . '" width="100" />';
        }
    }
}
