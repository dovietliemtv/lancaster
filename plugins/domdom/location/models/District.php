<?php namespace Domdom\Location\Models;

use Model;

/**
 * district Model
 */
class District extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_location_districts';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    public $rules = [
        'name_district' => 'required|unique:domdom_location_districts',
    ];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'location' => ['Domdom\Location\Models\Location', 'key' => 'location_district'],
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
