<?php namespace DomDom\Cms\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * News Pages Back-end Controller
 */
class NewsPages extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('DomDom.Cms', 'cms', 'newspages');
    }

    public function create() {
        BackendMenu::setContext('DomDom.Cms', 'cms', 'new_newspage');

        return $this->asExtension('FormController')->create();
    }

    public function listOverrideColumnValue($record, $columnName) {
        if ( $columnName == 'featured_image' ) {
            return '<img src="' . $record->featured_image->path . '" width="100" />';
        }
    }
}
