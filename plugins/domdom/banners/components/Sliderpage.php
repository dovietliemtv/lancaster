<?php namespace Domdom\Banners\Components;

use Cms\Classes\ComponentBase;
use DomDom\Banners\Models\Group;
use DomDom\Banners\Models\Banner;
use DomDom\Banners\Models\BannerGroup;

class Sliderpage extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Slider Page',
            'description' => 'No description provided yet...'
        ];
    }

    public function onRun()
    {
        $cp_grouppage = Group::where('code_group', $this->property('codesliderpage'))->first();

        if($cp_grouppage){
            $this->cp_sliderpage = Banner::whereIn( 'id', BannerGroup::where( 'group_id', $cp_grouppage->id )
                ->lists( 'banner_id' ) )
                ->orderByRaw("RAND()")->get();
        }
    }

    public function defineProperties()
    {
        return [
            'codesliderpage' => [
                'title'             => 'Code',
                'type'              => 'string'
            ]
        ];
    }

    public $cp_sliderpage;
}
