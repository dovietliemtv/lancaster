<?php namespace DomDom\Projects\Models;

use Model;

/**
 * Project Model
 */
class Project extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_projects_projects';

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    public $translatable = ['name_pj','slug_pj','short_description_pj','description_pj','quote_pj','title_feature_pj','name_feature_pj'];

    public $rules = [
        'name_pj' => 'required',
        'slug_pj' => 'required|unique:domdom_projects_projects',
        'featured_image_pj' => 'required',
        'featured_image_pj_1' => 'required'
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    protected $jsonable = ['feature_pj'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'categories' => [
            'Domdom\Projects\Models\Category',
            'table'    => 'domdom_projects_project_categories',
            'key' => 'project_id'
        ]
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [
        'featured_image_pj' => 'System\Models\File',
        'featured_image_pj_1' => 'System\Models\File',
        'featured_image_pj_2' => 'System\Models\File',
        'featured_image_design_pj' => 'System\Models\File',
    ];
    public $attachMany = [
        'gallery' => 'System\Models\File'
    ];
}
