<?php namespace DomDom\Cms\Models;

use Model;

/**
 * Member Model
 */
class AskAQuestion extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'domdom_projects_askaquestion';

    public $rules = [
        'name' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'type' => 'required',
        'message' => 'required',
    ];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = ['aaqt' => ['DomDom\Cms\Models\AskAQuestionType',
            'table' => 'domdom_projects_askaquestion_type', 'key' => 'type' ]];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}